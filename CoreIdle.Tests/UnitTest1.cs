using System;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows;
using CoreIdle.Character;
using CoreIdle.Inventory;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CoreIdle.Tests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            var text = File.ReadAllText("C:\\Users\\DGavrilichev\\source\\repos\\CoreIdle\\CoreIdle.Tests\\Text.txt");

            var lineLength = 0;
            var count = 0;
            var done = false;
            var rgx = new Regex("[^a-zA-Z0-9]");

            while (!done)
            {
                var start = text.IndexOf("<div>", StringComparison.Ordinal);
                var end = text.IndexOf("</div>", StringComparison.Ordinal);

                if (start == -1 || end == -1)
                {
                    done = true;
                }
                else
                {
                    var subText = text.Substring(start + 5, end - start - 5);
                    subText = rgx.Replace(subText, "");

                    if (lineLength++ < 10)
                        Console.Write($"\"{subText}\", ");
                    else
                    {
                        Console.WriteLine($"\"{subText}\", ");
                        lineLength = 0;
                    }

                    text = text.Remove(start, end - start + 6);
                    count++;
                }
            }
            Console.WriteLine($"Count: {count}");
        }

        [TestMethod]
        public void TestMethod3()
        {
            var text = File.ReadAllText("Text.txt");
            var all = text.Replace("\r", "").Split('\n');

            foreach (var s in all)
            {
                var all2 = s.Split(new char[] { ' ', '\t' });
                Console.WriteLine($@"{{{all2[0]}, {all2[1]}}},");
            }


        }

        [TestMethod]
        public void TestMethod2()
        {
            for (var i = 0; i < 50; i++)
            {
                Console.WriteLine(NameGenerator.NameGenerator.Generate());
            }
        }

        [TestMethod]
        public void TestMethod4()
        {
            Random random = new Random();
            var sw = new Stopwatch();
            sw.Start();
            Array values = Enum.GetValues(typeof(ItemId));

            for (int i = 0; i < 20; i++)
            {
                ItemId randomBar = (ItemId)values.GetValue(random.Next(values.Length));
                var item = EquipmentFactory.Create(randomBar, random.Next(17));
                Console.WriteLine($@"item.Id: {item}");
                Console.WriteLine($@"SW:{sw.Elapsed:G}");
                sw.Restart();
            }
        }


        [TestMethod]
        public void TestMethod5()
        {
            var human = PassiveEffectFactory.Create(UnitClass.ElvenMystic);
            foreach (var passiveEffectMultiLevel in human)
            {
                var effect = passiveEffectMultiLevel.GetCurrentEffect(80);
                if(effect != null)
                    Console.WriteLine(effect.Description);
            }
        }

        [TestMethod]
        public void TestMethod6()
        {
            
            for (var i = 0; i < 10; i++)
            {
                Console.WriteLine(BaseUnitGenerator.Get());
            }
        }
    }
}
