﻿// ReSharper disable All
namespace CoreIdle
{
    /// <summary>
    /// Const base attack speeed
    /// </summary>
    public static class ConstAttackSpeed
    {
        public const int VeryFast = 433;
        public const int Fast = 379;
        public const int Normal = 325;
        public const int Slow = 293;
        public const int VerySlow = 227;
        public const int NoWeapon = 300;
        public const int Bow = 150;
    }

    /// <summary>
    /// Const base weapon price
    /// </summary>
    public static class ConstPrice
    {
        public const int Ng17 = 17;
        public const int Ng69 = 69;
        public const int Ng384 = 384;
        public const int Ng4035 = 4035;
        public const int Ng6250 = 6250;
        public const int Ng9250 = 9250;
        public const int Ng12700 = 12700;
        public const int Ng27k = 27000;
        public const int Ng68k = 68000;
        public const int Ng122k = 122000;

        public const int D9k = 9000;
        public const int D205k = 205000;
        public const int D439k = 439000;
        public const int D484k = 484000;
    }

    /// <summary>
    /// Const crystal count
    /// </summary>
    public static class ConstCrystal
    {
        public const int D32 = 32;
        public const int D743 = 743;
        public const int D1594 = 1594;
        public const int D1758 = 1758;
    }
}
