﻿using System.Collections.Generic;
using System.Linq;
using CoreIdle.Character;

namespace CoreIdle
{
    internal static class GameServerStateFactory
    {
        internal static GameServerState CreateFirstServer()
        {
            var playerBase = Enumerable.Range(0, 100).Select(_ => BaseUnitGenerator.Get()).ToList();

            var result = new GameServerState
            {
                GoldDropRate = 1,
                ItemDropRate = 1, 
                XpRate = 1, 
                Name = "Local network server",
                PlayerSquad = new Squad
                {
                    Name = "My squad",
                    Members = playerBase.Take(10).ToList()
                },
                Players = playerBase
            };

            return result;
        }
    }
}
