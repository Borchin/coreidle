﻿using System.Windows;
using CoreIdle.Ui;

namespace CoreIdle
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App
    {
        private void AppOnStartup(object sender, StartupEventArgs e)
        {
            var viewModel = new MainViewModel();
            var window = new MainWindow(viewModel);
            window.Show();
        }
    }
}
