﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using JetBrains.Annotations;
using Polenter.Serialization;

namespace CoreIdle.DataAccess
{
    internal enum FileState
    {
        Ok = 0,
        AlreadyExist = 1,
        BadName = 2,
    }

    internal static class SaveHelper
    {
        private static readonly string SaveFolder = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "CoreIdle");
        private const string DbExt = ".cidb";
        private static readonly SharpSerializer Serializer = new SharpSerializer(new SharpSerializerBinarySettings(BinarySerializationMode.Burst));

        [NotNull]
        internal static List<string> GetDbList()
        {
            CreateSaveFolder();

            var files = Directory.GetFiles(SaveFolder, $"*{DbExt}", SearchOption.TopDirectoryOnly);
            var result = files.Select(f => new FileInfo(f).Name); 
            return result.Select(s => s.Replace(DbExt, string.Empty, StringComparison.InvariantCulture)).ToList();
        }

        internal static void Save([NotNull] GameState gameState, string name)
        {
            if(gameState == null) throw new ArgumentNullException(nameof(gameState));
            if (string.IsNullOrWhiteSpace(name)) throw new ArgumentNullException(nameof(name));

            Serializer.Serialize(gameState, GetFullName(name));
        }

        [NotNull]
        internal static GameState Load(string name)
        {
            if (string.IsNullOrWhiteSpace(name)) throw new ArgumentNullException(nameof(name));

            return (GameState)Serializer.Deserialize(GetFullName(name));
        }

        internal static FileState FileNameIsOk(string name)
        {
            if (name.IndexOfAny(Path.GetInvalidFileNameChars()) >= 0)
                return FileState.BadName;

            return File.Exists(GetFullName(name)) ? FileState.AlreadyExist : FileState.Ok;
        }

        internal static string GetFullName(string name)
        {
            if(string.IsNullOrWhiteSpace(name)) throw new ArgumentNullException(nameof(name));

            CreateSaveFolder();
            return Path.Combine(SaveFolder, $"{name}{DbExt}");
        }

        private static void CreateSaveFolder()
        {
            Directory.CreateDirectory(SaveFolder);
        }
    }
}
