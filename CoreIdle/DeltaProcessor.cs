﻿using System;

namespace CoreIdle
{
    internal sealed class DeltaProcessor
    {
        private readonly TimeSpan _timeToFire;
        private TimeSpan _current;

        internal DeltaProcessor(TimeSpan timeToFire)
        {
            _timeToFire = timeToFire;
        }

        internal bool Check(TimeSpan delta)
        {
            _current += delta;
            if (_current < _timeToFire) 
                return false;

            _current -= _timeToFire;
            return true;
        }
    }
}
