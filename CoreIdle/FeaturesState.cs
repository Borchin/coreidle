﻿using System;

namespace CoreIdle
{
    [Serializable]
    public sealed class FeaturesState
    {
        public bool GlobalUpgrades { get; set; }
        public bool SelectNewServer { get; set; }
        public bool ManageParty { get; set; }
        public bool ManageAdventures { get; set; }
    }
}
