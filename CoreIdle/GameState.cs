﻿using System;
using System.Collections.Generic;

namespace CoreIdle
{
    [Serializable]
    public sealed class GameState
    {
        public Dictionary<CurrencyType, int> Currency { get; set; } = new Dictionary<CurrencyType, int>();
        public GameServerState GameServer { get; set; }
        public FeaturesState Features { get; set; }
        public DateTime GameTime { get; set; }
    }
}
