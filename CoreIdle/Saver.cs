﻿using System;
using System.Diagnostics.CodeAnalysis;
using CoreIdle.DataAccess;

namespace CoreIdle
{
    internal sealed class Saver
    {
        private readonly string _saveName;
        [NotNull] private readonly GameState _gameState;
        [NotNull] private readonly DeltaProcessor _saveTimer = new DeltaProcessor(TimeSpan.FromSeconds(10));

        internal Saver(string saveName, [NotNull] GameState gameState)
        {
            if (string.IsNullOrWhiteSpace(saveName)) throw new ArgumentNullException(nameof(saveName));
            _saveName = saveName;
            _gameState = gameState ?? throw new ArgumentNullException(nameof(gameState));
        }

        internal void OnUpdate(TimeSpan delta)
        {
            if (_saveTimer.Check(delta))
            {
                SaveHelper.Save(_gameState, _saveName);
            }
        }
    }
}
