﻿using System;

namespace CoreIdle.Character
{
    internal static class CharacterPortraitProvider
    {
        private const string BasePath = "Images\\Portraits\\"; 

        internal static string GetPortraitPath(UnitClass unitClass)
        {
            return unitClass switch
            {
                UnitClass.HumanMystic => $"{BasePath}Human.jpg",
                UnitClass.ElvenMystic => $"{BasePath}Elf.jpg",
                UnitClass.DarkElvenMystic => $"{BasePath}DarkElf.jpg",
                UnitClass.OrcMystic => $"{BasePath}Orc.jpg",
                UnitClass.HumanFighter => $"{BasePath}Human.jpg",
                UnitClass.ElvenFighter => $"{BasePath}Elf.jpg",
                UnitClass.DarkElvenFighter => $"{BasePath}DarkElf.jpg",
                UnitClass.OrcFighter => $"{BasePath}Orc.jpg",
                UnitClass.DwarfFighter => $"{BasePath}Dwarf.jpg",
                _ => throw new ArgumentOutOfRangeException(nameof(unitClass), unitClass, null)
            };
        }
    }
}
