﻿namespace CoreIdle.Character
{
    public sealed class StatChange
    {
        internal Stat Stat { get; }
        internal StatCalcAction CalcAction { get; }
        internal double Value { get; }

        internal StatChange(Stat stat, StatCalcAction calcAction, double value)
        {
            Stat = stat;
            CalcAction = calcAction;
            Value = value;
        }
    }
}
