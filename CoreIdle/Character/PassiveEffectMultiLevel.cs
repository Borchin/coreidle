﻿using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;

namespace CoreIdle.Character
{
    internal interface IPassiveEffectProvider
    {
        IReadOnlyDictionary<int, PassiveEffect> All { get; }
        PassiveEffect GetCurrentEffect(int unitLevel);
    }

    internal sealed class PassiveEffectMultiLevel : IPassiveEffectProvider
    {
        [NotNull] private readonly Dictionary<int, PassiveEffect> _unitLevelToEffect;

        internal PassiveEffectMultiLevel([NotNull] Dictionary<int, PassiveEffect> unitLevelToEffect)
        {
            _unitLevelToEffect = unitLevelToEffect ?? throw new ArgumentNullException(nameof(unitLevelToEffect));
        }

        public IReadOnlyDictionary<int, PassiveEffect> All => _unitLevelToEffect;

        [CanBeNull]
        public PassiveEffect GetCurrentEffect(int unitLevel)
        {
            var fit = _unitLevelToEffect.Where(p => p.Key < unitLevel).OrderByDescending(p => p.Key).ToList();
            
            return fit.Any() ? fit.First().Value : null;
        }
    }

    internal sealed class PassiveEffectSingleLevel : IPassiveEffectProvider
    {
        [NotNull] private readonly PassiveEffect _effect;

        internal PassiveEffectSingleLevel([NotNull] PassiveEffect effect)
        {
            _effect = effect ?? throw new ArgumentNullException(nameof(effect));
            All = new Dictionary<int, PassiveEffect>{{1, _effect}};
        }

        public IReadOnlyDictionary<int, PassiveEffect> All { get; }

        [CanBeNull]
        public PassiveEffect GetCurrentEffect(int unitLevel)
        {
            return _effect;
        }
    }
}
