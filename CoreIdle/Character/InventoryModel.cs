﻿using System;
using System.Collections.Generic;
using CoreIdle.Inventory;
using CoreIdle.Ui;
using JetBrains.Annotations;

namespace CoreIdle.Character
{
    internal sealed class InventoryModel : ViewModelBase
    {
        [NotNull] private readonly Dictionary<InventorySlot, BaseItem> _baseInventory;
        [NotNull] private readonly List<BaseItem> _commonInventory;


        internal InventoryModel([NotNull] Dictionary<InventorySlot, BaseItem> baseInventory, [NotNull] List<BaseItem> commonInventory)
        {
            _baseInventory = baseInventory ?? throw new ArgumentNullException(nameof(baseInventory));
            _commonInventory = commonInventory ?? throw new ArgumentNullException(nameof(commonInventory));
        }
    }
}
