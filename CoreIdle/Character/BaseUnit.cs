﻿using System;
using System.Collections.Generic;
using CoreIdle.Inventory;

namespace CoreIdle.Character
{
    [Serializable]
    public sealed class BaseUnit
    {
        public string Name { get; set; }
        public string PortraitName { get; set; }
        public UnitClass UnitClass { get; set; }
        public UnitRace UnitRace { get; set; }
        public int Level { get; set; }
        public int ExpOnCurrentLevel { get; set; }
        public Dictionary<InventorySlot, BaseItem> EquippedItems { get; set; } = new Dictionary<InventorySlot, BaseItem>();

        public override string ToString()
        {
            return $"{Name} {UnitClass} (lvl {Level})";
        }
    }
}
