﻿using System;
using System.Collections.Generic;

namespace CoreIdle.Character
{
    [Serializable]
    public sealed class Squad
    {
        public string Name { get; set; }
        public List<BaseUnit> Members { get; set; } = new List<BaseUnit>();
        public List<BaseItem> CommonInventory { get; set; } = new List<BaseItem>();
    }
}
