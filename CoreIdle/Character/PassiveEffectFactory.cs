﻿using System;
using System.Collections.Generic;
using CoreIdle.Inventory;
using JetBrains.Annotations;

namespace CoreIdle.Character
{
    internal static class PassiveEffectFactory
    {
        [NotNull]
        internal static List<IPassiveEffectProvider> Create(UnitClass unitCLass)
        {
            var result = new List<IPassiveEffectProvider>();

            switch (unitCLass)
            {
                case UnitClass.HumanMystic:
                    var stats = new List<StatChange>
                    {
                        new StatChange(Stat.MaxHp, StatCalcAction.AddToBase, 25),
                        new StatChange(Stat.HpPerLevel, StatCalcAction.AddToBase, 5),
                        new StatChange(Stat.Matk, StatCalcAction.Multiply, 1.08),
                        new StatChange(Stat.MCritChance, StatCalcAction.Multiply, 1.04),
                        new StatChange(Stat.CastSpeed, StatCalcAction.Multiply, 1.15),
                        new StatChange(Stat.StunResist, StatCalcAction.AddToBase, 10),
                        new StatChange(Stat.WaterResist, StatCalcAction.AddToBase, 15),
                    };
                    var effect = new PassiveEffect("Human Mystic", "race-icon_human.svg", GetDescription(stats), null, stats);
                    result.Add(new PassiveEffectMultiLevel(new Dictionary<int, PassiveEffect>
                    {
                        {1, effect}
                    }));
                    result.AddRange(GetMysticPassives());
                    break;

                case UnitClass.ElvenMystic:
                    stats = new List<StatChange>
                    {
                        new StatChange(Stat.MaxHp, StatCalcAction.AddToBase, 10),
                        new StatChange(Stat.HpPerLevel, StatCalcAction.AddToBase, 4),
                        new StatChange(Stat.Matk, StatCalcAction.Multiply, 1.02),
                        new StatChange(Stat.Mdef, StatCalcAction.Multiply, 1.02),
                        new StatChange(Stat.Evasion, StatCalcAction.AddToBase, 3),
                        new StatChange(Stat.MCritChance, StatCalcAction.Multiply, 1.1),
                        new StatChange(Stat.CastSpeed, StatCalcAction.Multiply, 1.2),
                        new StatChange(Stat.HolyResist, StatCalcAction.AddToBase, 15),
                        new StatChange(Stat.ShieldBlockChance, StatCalcAction.Multiply, 1.1),
                    };
                    effect = new PassiveEffect("Elven Mystic", "race-icon_elf.svg", GetDescription(stats), null, stats);
                    result.Add(new PassiveEffectMultiLevel(new Dictionary<int, PassiveEffect>
                    {
                        {1, effect}
                    }));
                    result.AddRange(GetMysticPassives());
                    break;

                case UnitClass.DarkElvenMystic:
                    stats = new List<StatChange>
                    {
                        new StatChange(Stat.HpPerLevel, StatCalcAction.AddToBase, 3),
                        new StatChange(Stat.Matk, StatCalcAction.Multiply, 1.15),
                        new StatChange(Stat.Mdef, StatCalcAction.Multiply, 1.02),
                        new StatChange(Stat.Evasion, StatCalcAction.AddToBase, 2),
                        new StatChange(Stat.MCritChance, StatCalcAction.Multiply, 1.02),
                        new StatChange(Stat.CastSpeed, StatCalcAction.Multiply, 1.1),
                        new StatChange(Stat.DarkResist, StatCalcAction.AddToBase, 15),
                        new StatChange(Stat.ShieldBlockChance, StatCalcAction.Multiply, 1.1),
                    };
                    effect = new PassiveEffect("Dark Elven Mystic", "race-icon_dark.svg", GetDescription(stats), null, stats);
                    result.Add(new PassiveEffectMultiLevel(new Dictionary<int, PassiveEffect>
                    {
                        {1, effect}
                    }));
                    result.AddRange(GetMysticPassives());
                    break;

                case UnitClass.OrcMystic:
                    stats = new List<StatChange>
                    {
                        new StatChange(Stat.MaxHp, StatCalcAction.AddToBase, 40),
                        new StatChange(Stat.HpPerLevel, StatCalcAction.AddToBase, 8),
                        new StatChange(Stat.Mdef, StatCalcAction.Multiply, 1.15),
                        new StatChange(Stat.FireResist, StatCalcAction.AddToBase, 15),
                        new StatChange(Stat.StunResist, StatCalcAction.AddToBase, 20),
                        new StatChange(Stat.HpRecovery, StatCalcAction.AddToBase, 5),
                    };
                    effect = new PassiveEffect("Orc Mystic", "race-icon_orc.svg", GetDescription(stats), null, stats);
                    result.Add(new PassiveEffectMultiLevel(new Dictionary<int, PassiveEffect>
                    {
                        {1, effect}
                    }));
                    result.AddRange(GetMysticPassives());
                    break;

                case UnitClass.HumanFighter:
                    stats = new List<StatChange>
                    {
                        new StatChange(Stat.MaxHp, StatCalcAction.AddToBase, 40),
                        new StatChange(Stat.HpPerLevel, StatCalcAction.AddToBase, 9),
                        new StatChange(Stat.Patk, StatCalcAction.Multiply, 1.04),
                        new StatChange(Stat.Evasion, StatCalcAction.AddToBase, 2),
                        new StatChange(Stat.AtkSpeed, StatCalcAction.Multiply, 1.02),
                        new StatChange(Stat.StunResist, StatCalcAction.AddToBase, 20),
                        new StatChange(Stat.WaterResist, StatCalcAction.AddToBase, 15),
                    };
                    effect = new PassiveEffect("Human Fighter", "race-icon_human.svg", GetDescription(stats), null, stats);
                    result.Add(new PassiveEffectMultiLevel(new Dictionary<int, PassiveEffect>
                    {
                        {1, effect}
                    }));
                    result.AddRange(GetFighterPassives());
                    break;

                case UnitClass.ElvenFighter:
                    stats = new List<StatChange>
                    {
                        new StatChange(Stat.MaxHp, StatCalcAction.AddToBase, 30),
                        new StatChange(Stat.HpPerLevel, StatCalcAction.AddToBase, 7),
                        new StatChange(Stat.Patk, StatCalcAction.Multiply, 1.02),
                        new StatChange(Stat.Evasion, StatCalcAction.AddToBase, 6),
                        new StatChange(Stat.AtkSpeed, StatCalcAction.Multiply, 1.1),
                        new StatChange(Stat.StunResist, StatCalcAction.AddToBase, 5),
                        new StatChange(Stat.HolyResist, StatCalcAction.AddToBase, 15),
                        new StatChange(Stat.ShieldBlockChance, StatCalcAction.Multiply, 1.1),
                    };
                    effect = new PassiveEffect("Elven Fighter", "race-icon_elf.svg", GetDescription(stats), null, stats);
                    result.Add(new PassiveEffectMultiLevel(new Dictionary<int, PassiveEffect>
                    {
                        {1, effect}
                    }));
                    result.AddRange(GetFighterPassives());
                    break;

                case UnitClass.DarkElvenFighter:
                    stats = new List<StatChange>
                    {
                        new StatChange(Stat.MaxHp, StatCalcAction.AddToBase, 20),
                        new StatChange(Stat.HpPerLevel, StatCalcAction.AddToBase, 5),
                        new StatChange(Stat.Patk, StatCalcAction.Multiply, 1.1),
                        new StatChange(Stat.Evasion, StatCalcAction.AddToBase, 4),
                        new StatChange(Stat.AtkSpeed, StatCalcAction.Multiply, 1.08),
                        new StatChange(Stat.DarkResist, StatCalcAction.AddToBase, 15),
                        new StatChange(Stat.ShieldBlockChance, StatCalcAction.Multiply, 1.1),
                    };
                    effect = new PassiveEffect("Elven Fighter", "race-icon_dark.svg", GetDescription(stats), null, stats);
                    result.Add(new PassiveEffectMultiLevel(new Dictionary<int, PassiveEffect>
                    {
                        {1, effect}
                    }));
                    result.AddRange(GetFighterPassives());
                    break;

                case UnitClass.OrcFighter:
                    stats = new List<StatChange>
                    {
                        new StatChange(Stat.MaxHp, StatCalcAction.AddToBase, 55),
                        new StatChange(Stat.HpPerLevel, StatCalcAction.AddToBase, 12),
                        new StatChange(Stat.Patk, StatCalcAction.Multiply, 1.08),
                        new StatChange(Stat.StunResist, StatCalcAction.AddToBase, 30),
                        new StatChange(Stat.FireResist, StatCalcAction.AddToBase, 15),
                        new StatChange(Stat.HpRecovery, StatCalcAction.AddToBase, 5),
                    };
                    effect = new PassiveEffect("Orc Fighter", "race-icon_orc.svg", GetDescription(stats), null, stats);
                    result.Add(new PassiveEffectMultiLevel(new Dictionary<int, PassiveEffect>
                    {
                        {1, effect}
                    }));
                    result.AddRange(GetFighterPassives());
                    break;

                case UnitClass.DwarfFighter:
                    stats = new List<StatChange>
                    {
                        new StatChange(Stat.MaxHp, StatCalcAction.AddToBase, 70),
                        new StatChange(Stat.HpPerLevel, StatCalcAction.AddToBase, 13),
                        new StatChange(Stat.StunResist, StatCalcAction.AddToBase, 50),
                        new StatChange(Stat.WindResist, StatCalcAction.AddToBase, 15),
                        new StatChange(Stat.HpRecovery, StatCalcAction.AddToBase, 5),
                    };
                    effect = new PassiveEffect("Dwarf Fighter", "race-icon_dwarf.svg", GetDescription(stats), null, stats);
                    result.Add(new PassiveEffectMultiLevel(new Dictionary<int, PassiveEffect>
                    {
                        {1, effect}
                    }));
                    result.AddRange(GetFighterPassives());
                    break;

                default:
                    throw new ArgumentOutOfRangeException(nameof(unitCLass), unitCLass, null);
            }

            return result;
        }

        [NotNull]
        private static List<IPassiveEffectProvider> GetFighterPassives()
        {
            var result = new List<IPassiveEffectProvider>();
            
            result.Add(CreateSimpleMultiEffect("Armor mastery (Fighter)", 
                "armor_mastery.svg", Stat.Pdef, StatCalcAction.AddToBase,
                new Dictionary<int, double>
                {
                    {4, 9},
                    {7, 12},
                    {10, 15},
                    {13, 18},
                    {16, 21},
                }));

            result.Add(CreateSimpleMultiEffect("Weapon mastery (Fighter)",
                "weapon-mastery.svg", Stat.Patk, StatCalcAction.AddToBase,
                new Dictionary<int, double>
                {
                    {4, 2},
                    {10, 4},
                    {16, 6},
                }));

            return result;
        }

        [NotNull]
        private static List<IPassiveEffectProvider> GetMysticPassives()
        {
            var result = new List<IPassiveEffectProvider>();
            
            result.Add(CreateSimpleMultiEffect("Armor mastery (Mystic)", 
                "armor_mastery.svg", Stat.Pdef, StatCalcAction.AddToBase,
                new Dictionary<int, double>
                {
                    {5, 7},
                    {10, 8},
                    {15, 9},
                }));

            //Weapon mastery
            var stats = new List<StatChange>
            {
                new StatChange(Stat.Patk, StatCalcAction.AddToBase, 2),
                new StatChange(Stat.Matk, StatCalcAction.AddToBase, 2),
            };
            var effect1 = new PassiveEffect("Weapon mastery (Mystic)", "armor_mastery.svg", GetDescription(stats), 1, stats);
            stats = new List<StatChange>
            {
                new StatChange(Stat.Patk, StatCalcAction.AddToBase, 3),
                new StatChange(Stat.Matk, StatCalcAction.AddToBase, 4),
            };
            var effect2 = new PassiveEffect("Weapon mastery (Mystic)", "armor_mastery.svg", GetDescription(stats), 2, stats);
            stats = new List<StatChange>
            {
                new StatChange(Stat.Patk, StatCalcAction.AddToBase, 4),
                new StatChange(Stat.Matk, StatCalcAction.AddToBase, 6),
            };
            var effect3 = new PassiveEffect("Weapon mastery (Mystic)", "armor_mastery.svg", GetDescription(stats), 3, stats);
            result.Add(new PassiveEffectMultiLevel(new Dictionary<int, PassiveEffect>
            {
                {7, effect1},
                {12, effect2},
                {17, effect3},
            }));

            //Spellcraft
            stats = new List<StatChange>
            {
                new StatChange(Stat.AtkSpeed, StatCalcAction.Multiply, 1.2),
                new StatChange(Stat.CastSpeed, StatCalcAction.Multiply, 1.5),
            };
            var spellcraft = new ArmorDependentEffect("Spellcraft", "spellcraft.svg", GetDescription(stats), null, stats, ArmorKind.Magic);
            result.Add(new PassiveEffectSingleLevel(spellcraft));

            return result;
        }

        [NotNull]
        private static PassiveEffectMultiLevel CreateSimpleMultiEffect(string name, string icon, Stat stat, StatCalcAction action, [NotNull] Dictionary<int, double> statLevelToValue)
        {
            if (statLevelToValue == null) throw new ArgumentNullException(nameof(statLevelToValue));

            var effects = new Dictionary<int, PassiveEffect>();
            var skillLevel = 0;
            foreach (var (requiredLevel, value) in statLevelToValue)
            {
                skillLevel++;
                var stats = new List<StatChange>
                {
                    new StatChange(stat, action, value),
                };
                var effect = new PassiveEffect(name, icon, GetDescription(stats), skillLevel, stats);
                effects.Add(requiredLevel, effect);
            }

            return new PassiveEffectMultiLevel(effects);
        }

        private static string GetDescription([NotNull] List<StatChange> statChanges)
        {
            if (statChanges == null) throw new ArgumentNullException(nameof(statChanges));

            var result = string.Empty;

            foreach (var statChange in statChanges)
            {
                var statName = TextHelper.GetStatName(statChange.Stat);

                var value = statChange.CalcAction == StatCalcAction.AddToBase
                    ? $"{statChange.Value:0.##}"
                    : $"{((statChange.Value - 1) * 100):0.##}%";

                var subString = (statChange.CalcAction == StatCalcAction.AddToBase && statChange.Value > 0) 
                                || (statChange.CalcAction == StatCalcAction.Multiply && statChange.Value > 1)
                    ? "+"
                    : string.Empty;

                result += $"{statName}: {subString}{value}\n";
            }

            return result;
        }
    }
}
