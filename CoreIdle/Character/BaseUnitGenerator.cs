﻿using System;
using System.Collections.Generic;
using CoreIdle.Inventory;
using JetBrains.Annotations;

namespace CoreIdle.Character
{
    internal static class BaseUnitGenerator
    {
        private static readonly Random Rnd = new Random(Guid.NewGuid().GetHashCode());
        private static readonly List<UnitGenerationInfo> MetaData = new List<UnitGenerationInfo>
        {
            new UnitGenerationInfo(UnitRace.Human, UnitClass.HumanMystic),
            new UnitGenerationInfo(UnitRace.Human, UnitClass.HumanFighter),
            new UnitGenerationInfo(UnitRace.Elf, UnitClass.ElvenMystic),
            new UnitGenerationInfo(UnitRace.Elf, UnitClass.ElvenFighter),
            new UnitGenerationInfo(UnitRace.DarkElf, UnitClass.DarkElvenFighter),
            new UnitGenerationInfo(UnitRace.DarkElf, UnitClass.DarkElvenMystic),
            new UnitGenerationInfo(UnitRace.Orc, UnitClass.OrcFighter),
            new UnitGenerationInfo(UnitRace.Orc, UnitClass.OrcMystic),
            new UnitGenerationInfo(UnitRace.Dwarf, UnitClass.DwarfFighter)
        };

        [NotNull]
        internal static BaseUnit Get()
        {
            var baseInfo = MetaData[Rnd.Next(MetaData.Count)];
            var result = new BaseUnit
            {
                ExpOnCurrentLevel = 0,
                Level = 1,
                Name = NameGenerator.NameGenerator.Generate(),
                PortraitName = CharacterPortraitProvider.GetPortraitPath(baseInfo.UnitClass),
                UnitClass = baseInfo.UnitClass,
                UnitRace = baseInfo.UnitRace
            };

            //todo: remake to real equipment
            result.EquippedItems.Add(InventorySlot.Body, new BaseItem
            {
                EnchantLevel = 0,
                GuidKey = Guid.NewGuid(),
                Id = ItemId.ApprenticeTunic
            });

            result.EquippedItems.Add(InventorySlot.Boots, new BaseItem
            {
                EnchantLevel = 0,
                GuidKey = Guid.NewGuid(),
                Id = ItemId.LeatherBoots
            });

            return result;
        }

        private sealed class UnitGenerationInfo
        {
            internal UnitGenerationInfo(UnitRace unitRace, UnitClass unitClass)
            {
                UnitRace = unitRace;
                UnitClass = unitClass;
            }

            internal UnitRace UnitRace { get; }
            internal UnitClass UnitClass { get; }
        }
    }
}
