﻿namespace CoreIdle.Character
{
    public enum Stat
    {
        MaxHp,
        HpPerLevel,
        CurrentHp,
        Patk,
        Matk,
        Pdef,
        Mdef,
        Accuracy,
        Evasion,
        PCritChance,
        PCritPower,
        MCritChance,
        MCritPower,
        AtkSpeed,
        CastSpeed,
        InterruptResist,
        StunResist,
        DebuffResist,
        FireResist,
        WaterResist,
        WindResist,
        DarkResist,
        HolyResist,
        PoisonResist,
        BleedResist,
        HealingBonus,
        HealReceivedBonus,
        HpRecovery,
        ShieldBlockValue,
        ShieldBlockChance,
        CooldownRecoveryMultiply,
    }

    public enum StatCalcAction
    {
        /// <summary>
        /// Adds flat value to base.
        /// </summary>
        AddToBase,

        /// <summary>
        /// Sums with other multiply bonuses. (Sum + 1) * (base value + AddToBase) = result.
        /// </summary>
        Multiply
    }

    public enum UnitRace
    {
        Human,
        Dwarf,
        Elf,
        DarkElf,
        Orc,
        Undead,
        Construct,
        Beast,
        Animal,
        Plant,
        Humanoid,
        Spirit,
        Divine,
        Demonic,
        Dragon,
        Giant,
        Bug,
        Fairy
    }

    public enum UnitClass
    {
        HumanMystic,
        ElvenMystic,
        DarkElvenMystic,
        OrcMystic,
        HumanFighter,
        ElvenFighter,
        DarkElvenFighter,
        OrcFighter,
        DwarfFighter,
    }

    public enum EffectSource
    {
        Physical,
        Magic,
        Ranged,
        Melee,
        Bow,
        Dagger,
        Blunt,
        Fire,
        Water,
        Wind,
        Dark,
        Holy,
        Poison,
        Bleed,
        Debuff,
        Stun
    }
}
