﻿using System;
using System.Collections.Generic;
using CoreIdle.Inventory;
using JetBrains.Annotations;

namespace CoreIdle.Character
{
    internal class PassiveEffect
    {
        public PassiveEffect(string name, string icon, string description, int? level, [NotNull] List<StatChange> statChanges)
        {
            if(string.IsNullOrWhiteSpace(name)) throw new ArgumentNullException(nameof(name));
            if(string.IsNullOrWhiteSpace(icon)) throw new ArgumentNullException(nameof(icon));
            if(string.IsNullOrWhiteSpace(description)) throw new ArgumentNullException(nameof(description));
            Name = name;
            Icon = icon;
            Description = description;
            Level = level;
            StatChanges = statChanges ?? throw new ArgumentNullException(nameof(statChanges));
        }

        internal string Name { get; }
        internal string Icon { get; }
        internal string Description { get; }
        internal int? Level { get; }
        [NotNull] internal List<StatChange> StatChanges { get; }
    }

    internal class ArmorDependentEffect : PassiveEffect
    {
        internal ArmorKind RequiredArmor { get; }

        public ArmorDependentEffect(string name, string icon, string description, int? level, [NotNull] List<StatChange> statChanges, ArmorKind requiredArmor) 
            : base(name, icon, description, level, statChanges)
        {
            RequiredArmor = requiredArmor;
        }
    }

    internal class WeaponDependentEffect : PassiveEffect
    {
        [NotNull] internal Type RequiredWeapon { get; }

        public WeaponDependentEffect(string name, string icon, string description, int? level, [NotNull] List<StatChange> statChanges, [NotNull] Type requiredWeapon)
            : base(name, icon, description, level, statChanges)
        {
            RequiredWeapon = requiredWeapon;
        }
    }
}
