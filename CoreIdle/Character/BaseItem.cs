﻿using System;
using CoreIdle.Inventory;

namespace CoreIdle.Character
{
    [Serializable]
    public sealed class BaseItem
    {
        public Guid GuidKey { get; set; }
        public ItemId Id { get; set; }
        public int EnchantLevel { get; set; }

        public override string ToString()
        {
            var enchantLevel = EnchantLevel == 0 ? string.Empty : $"+{EnchantLevel}";
            return $"{Id} {EnchantLevel}";
        }
    }
}
