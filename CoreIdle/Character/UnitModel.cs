﻿using System;
using System.Collections.Generic;
using System.Globalization;
using CoreIdle.Ui;
using JetBrains.Annotations;

namespace CoreIdle.Character
{
    internal sealed class UnitModel : ViewModelBase
    {
        [NotNull] private readonly Dictionary<Stat, double> _baseStats;
        [NotNull] private readonly List<IPassiveEffectProvider> _passiveEffects;
        [NotNull] private BaseUnit _baseUnit;
        [NotNull] private readonly List<PassiveEffect> _buffs = new List<PassiveEffect>();

        [NotNull] internal Dictionary<Stat, double> TotalStats { get; } = new Dictionary<Stat, double>();

        public string Name { get; }
        public string Race { get; }
        public string Class { get; }
        public Uri Portrait { get; }

        private int _level;
        public int Level
        {
            get => _level;
            private set
            {
                if (_level == value) return;
                _level = value;
                OnPropertyChanged(() => Level);
            }
        }

        private string _currentExpPercent;
        public string CurrentExpPercent
        {
            get => _currentExpPercent;
            private set
            {
                if (_currentExpPercent == value) return;
                _currentExpPercent = value;
                OnPropertyChanged(() => CurrentExpPercent);
            }
        }

        private int _currentExp;
        public int CurrentExp
        {
            get => _currentExp;
            private set
            {
                if (_currentExp == value) return;
                _currentExp = value;
                OnPropertyChanged(() => CurrentExp);
            }
        }

        private int _expToNextLevel;
        public int ExpToNextLevel
        {
            get => _expToNextLevel;
            private set
            {
                if (_expToNextLevel == value) return;
                _expToNextLevel = value;
                OnPropertyChanged(() => ExpToNextLevel);
            }
        }

        internal UnitModel([NotNull] BaseUnit baseUnit, [NotNull] Dictionary<Stat, double> baseStats, [NotNull] List<IPassiveEffectProvider> passiveEffects)
        {
            _baseUnit = baseUnit ?? throw new ArgumentNullException(nameof(baseUnit));
            _baseStats = baseStats ?? throw new ArgumentNullException(nameof(baseStats));
            _passiveEffects = passiveEffects ?? throw new ArgumentNullException(nameof(passiveEffects));

            Name = baseUnit.Name;
            Race = TextHelper.GetRaceName(baseUnit.UnitRace);
            Class = TextHelper.GetClassName(baseUnit.UnitClass);
            Portrait = new Uri(CharacterPortraitProvider.GetPortraitPath(baseUnit.UnitClass));

            CalculateStats();
        }


        internal void CalculateStats()
        {
            CheckLevelUp();
            Level = _baseUnit.Level;
            CurrentExp = _baseUnit.ExpOnCurrentLevel;
            ExpToNextLevel = Experience.GetExpForNextLevel(_baseUnit.Level) ?? 0;
            CurrentExpPercent = ((decimal)ExpToNextLevel / CurrentExp * 100.0M).ToString("F:2", CultureInfo.InvariantCulture);
        }

        private void CheckLevelUp()
        {
            for (var expToLevel = Experience.GetExpForNextLevel(_baseUnit.Level); 
                expToLevel.HasValue && _baseUnit.ExpOnCurrentLevel >= expToLevel;
                expToLevel = Experience.GetExpForNextLevel(_baseUnit.Level))
            {
                _baseUnit.ExpOnCurrentLevel -= expToLevel.Value;
                _baseUnit.Level++;
                //todo: on level up
            }
        }
    }
}
