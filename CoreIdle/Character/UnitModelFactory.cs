﻿using System;
using System.Collections.Generic;
using JetBrains.Annotations;

namespace CoreIdle.Character
{
    internal static class UnitModelFactory
    {
        [NotNull] 
        private static readonly Dictionary<Stat, double> BaseStats = new Dictionary<Stat, double>
        {
            {Stat.MaxHp, 100},
            {Stat.HpPerLevel, 10},
            {Stat.Patk, 1},
            {Stat.Matk, 1},
            {Stat.Accuracy, 0},
            {Stat.Evasion, 0},
            {Stat.PCritChance, 0},
            {Stat.MCritChance, 5},
            {Stat.PCritPower, 2},
            {Stat.PCritPower, 2},
            {Stat.AtkSpeed, ConstAttackSpeed.NoWeapon},
            {Stat.CastSpeed, 200},
            {Stat.InterruptResist, 20},
            {Stat.StunResist, 0},
            {Stat.DebuffResist, 20},
            {Stat.FireResist, 0},
            {Stat.WaterResist, 0},
            {Stat.WindResist, 0},
            {Stat.DarkResist, 0},
            {Stat.HolyResist, 0},
            {Stat.PoisonResist, 0},
            {Stat.BleedResist, 0},
            {Stat.BleedResist, 0},
            {Stat.HealReceivedBonus, 0},
            {Stat.HpRecovery, 0},
            {Stat.ShieldBlockValue, 0},
            {Stat.ShieldBlockChance, 0},
            {Stat.CooldownRecoveryMultiply, 1},
        };

        [NotNull]
        internal static UnitModel Create([NotNull] BaseUnit baseUnit)
        {
            if(baseUnit == null) throw new ArgumentNullException(nameof(baseUnit));

            var unit = new UnitModel(baseUnit, BaseStats, PassiveEffectFactory.Create(baseUnit.UnitClass));
            

            return unit;
        }
    }
}
