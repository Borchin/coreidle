﻿using System;
using System.Diagnostics.CodeAnalysis;
using CoreIdle.Ui;

namespace CoreIdle
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        internal MainWindow([NotNull] MainViewModel viewModel)
        {
            if(viewModel == null) throw new ArgumentNullException(nameof(viewModel));
            InitializeComponent();
            DataContext = viewModel;
        }
    }
}
