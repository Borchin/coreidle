﻿using System.Diagnostics.CodeAnalysis;
using System.Windows.Input;

namespace CoreIdle.Ui
{
    internal sealed class SelectNewServerViewModel : ContentPageBase
    {
        private readonly MainManager _mainManager;

        private bool _startIsEnabled;
        public bool StartIsEnabled
        {
            get => _startIsEnabled;
            set
            {
                if (_startIsEnabled == value) return;
                _startIsEnabled = value;
                OnPropertyChanged(() => StartIsEnabled);
            }
        }

        internal SelectNewServerViewModel(string title, [NotNull] MainManager mainManager) : base(title)
        {
            _mainManager = mainManager;
            StartIsEnabled = mainManager.CurrentGameState.GameServer == null;
        }

        public ICommand StartCommand
        {
            get
            {
                return new UiCommand(o =>
                {
                    var server = GameServerStateFactory.CreateFirstServer();
                    _mainManager.PickServer(server);
                    StartIsEnabled = false;
                });
            }
        }
    }
}
