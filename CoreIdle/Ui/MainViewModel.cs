﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace CoreIdle.Ui
{
    internal sealed class MainViewModel : ViewModelBase
    {
        [NotNull] 
        private ViewModelBase _currentContent;
        [NotNull]
        public ViewModelBase CurrentContent
        {
            get => _currentContent;
            set
            {
                if (_currentContent == value) return;
                _currentContent = value;
                OnPropertyChanged(() => CurrentContent);
            }
        }

        internal MainViewModel()
        {
            var gameStartViewModel = new GameStartViewModel();
            gameStartViewModel.GameStart += OnGameStart;
            CurrentContent = gameStartViewModel;
        }

        private void OnGameStart(object sender, [NotNull] MainManager mainManager)
        {
            if(mainManager == null) throw new ArgumentNullException(nameof(mainManager));

            CurrentContent = new GameContentViewModel(mainManager);
        }
    }
}
