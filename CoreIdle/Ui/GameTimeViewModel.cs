﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Input;
using JetBrains.Annotations;

namespace CoreIdle.Ui
{
    internal sealed class GameTimeViewModel : ViewModelBase
    {
        [NotNull] private readonly MainManager _mainManager;
        
        private string _dateText = string.Empty;
        public string DateText
        {
            get => _dateText;
            set
            {
                if (_dateText == value) return;
                _dateText = value;
                OnPropertyChanged(() => DateText);
            }
        }

        private Visibility _playVisibility;
        public Visibility PlayVisibility
        {
            get => _playVisibility;
            set
            {
                if (_playVisibility == value) return;
                _playVisibility = value;
                OnPropertyChanged(() => PlayVisibility);
            }
        }

        private Visibility _stopVisibility;
        public Visibility StopVisibility
        {
            get => _stopVisibility;
            set
            {
                if (_stopVisibility == value) return;
                _stopVisibility = value;
                OnPropertyChanged(() => StopVisibility);
            }
        }

        internal GameTimeViewModel([NotNull] MainManager mainManager)
        {
            _mainManager = mainManager ?? throw new ArgumentNullException(nameof(mainManager));
            _mainManager.GameLoopManager.Update += OnUpdate;
            ManageControls();
        }

        public ICommand StopCommand
        {
            get
            {
                return new UiCommand(o =>
                {
                    _mainManager.GameLoopManager.GameIsPaused = true;
                    ManageControls();
                });
            }
        }

        public ICommand PlayCommand
        {
            get
            {
                return new UiCommand(o =>
                {
                    _mainManager.GameLoopManager.GameIsPaused = false;
                    ManageControls();
                });
            }
        }

        private void OnUpdate(object sender, TimeSpan deltaTime)
        {
            _mainManager.CurrentGameState.GameTime += deltaTime * 60 * 5;
            DateText = _mainManager.CurrentGameState.GameTime.ToString("f", CultureInfo.InvariantCulture);
        }

        private void ManageControls()
        {
            if (_mainManager.GameLoopManager.GameIsPaused)
            {
                PlayVisibility = Visibility.Visible;
                StopVisibility = Visibility.Hidden;
            }
            else
            {
                PlayVisibility = Visibility.Hidden;
                StopVisibility = Visibility.Visible;
            }
        }
    }
}
