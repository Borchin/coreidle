﻿using System;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Input;
using CoreIdle.DataAccess;
using JetBrains.Annotations;


namespace CoreIdle.Ui
{
    internal sealed class GameStartViewModel : ViewModelBase
    {
        internal event EventHandler<MainManager> GameStart;
        private void OnGameStart([NotNull] MainManager gameState)
        {
            if(gameState == null) throw new ArgumentNullException(nameof(gameState));

            GameStart?.Invoke(this, gameState);
        }

        private Visibility _startNewGameVisibility = Visibility.Hidden;
        public Visibility StartNewGameVisibility
        {
            get => _startNewGameVisibility;
            set
            {
                if (_startNewGameVisibility == value) return;
                _startNewGameVisibility = value;
                OnPropertyChanged(() => StartNewGameVisibility);
            }
        }

        private Visibility _loadGameVisibility = Visibility.Hidden;
        public Visibility LoadGameVisibility
        {
            get => _loadGameVisibility;
            set
            {
                if (_loadGameVisibility == value) return;
                _loadGameVisibility = value;
                OnPropertyChanged(() => LoadGameVisibility);
            }
        }

        private Visibility _selectModeVisibility = Visibility.Visible;
        public Visibility SelectModeVisibility
        {
            get => _selectModeVisibility;
            set
            {
                if (_selectModeVisibility == value) return;
                _selectModeVisibility = value;
                OnPropertyChanged(() => SelectModeVisibility);
            }
        }

        private Visibility _errorTextVisibility = Visibility.Visible;
        public Visibility ErrorTextVisibility
        {
            get => _errorTextVisibility;
            set
            {
                if (_errorTextVisibility == value) return;
                _errorTextVisibility = value;
                OnPropertyChanged(() => ErrorTextVisibility);
            }
        }

        private string _errorText = string.Empty;
        public string ErrorText
        {
            get => _errorText;
            set
            {
                if (_errorText == value) return;
                _errorText = value;
                OnPropertyChanged(() => ErrorText);
            }
        }

        private string _newGameName;
        public string NewGameName
        {
            get => _newGameName;
            set
            {
                if (_newGameName == value) return;
                _newGameName = value;
                CheckName();
                OnPropertyChanged(() => NewGameName);
            }
        }


        private bool _startNewGameIsEnabled;
        public bool StartNewGameIsEnabled
        {
            get => _startNewGameIsEnabled;
            set
            {
                if (_startNewGameIsEnabled == value) return;
                _startNewGameIsEnabled = value;
                OnPropertyChanged(() => StartNewGameIsEnabled);
            }
        }

        private bool _loadGameIsEnabled;
        public bool LoadGameIsEnabled
        {
            get => _loadGameIsEnabled;
            set
            {
                if (_loadGameIsEnabled == value) return;
                _loadGameIsEnabled = value;
                OnPropertyChanged(() => LoadGameIsEnabled);
            }
        }

        private int _selectedGameToLoad;
        public int SelectedGameToLoad
        {
            get => _selectedGameToLoad;
            set
            {
                if (_selectedGameToLoad == value) return;
                _selectedGameToLoad = value;
                LoadGameIsEnabled = _selectedGameToLoad != -1;
                OnPropertyChanged(() => SelectedGameToLoad);
            }
        }

        public ObservableCollection<string> SavedGames { get; } = new ObservableCollection<string>();

        private void CheckName()
        {
            var checkResult =  SaveHelper.FileNameIsOk(NewGameName);
            switch (checkResult)
            {
                case FileState.Ok:
                    StartNewGameIsEnabled = true;
                    ErrorTextVisibility = Visibility.Hidden;
                    break;
                case FileState.AlreadyExist:
                    StartNewGameIsEnabled = false;
                    ErrorText = "This name is already used. Think again.";
                    ErrorTextVisibility = Visibility.Visible;
                    break;
                case FileState.BadName:
                    StartNewGameIsEnabled = false;
                    ErrorText = "This name contains forbidden characters.";
                    ErrorTextVisibility = Visibility.Visible;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(checkResult), @"Woops!");
            }
        }

        public ICommand NewGameMenuCommand
        {
            get
            {
                return new UiCommand(o =>
                {
                    SelectModeVisibility = Visibility.Hidden;
                    LoadGameVisibility = Visibility.Hidden;
                    StartNewGameVisibility = Visibility.Visible;
                    NewGameName = NameGenerator.NameGenerator.Generate();
                });
            }
        }

        public ICommand LoadGameMenuCommand
        {
            get
            {
                return new UiCommand(o =>
                {
                    SelectModeVisibility = Visibility.Hidden;
                    LoadGameVisibility = Visibility.Visible;
                    StartNewGameVisibility = Visibility.Hidden;
                    SavedGames.Clear();
                    var saves = SaveHelper.GetDbList();

                    foreach (var savedGame in saves)
                        SavedGames.Add(savedGame);

                    SelectedGameToLoad = -1;
                });
            }
        }

        public ICommand BackMenuCommand
        {
            get
            {
                return new UiCommand(o =>
                {
                    SelectModeVisibility = Visibility.Visible;
                    LoadGameVisibility = Visibility.Hidden;
                    StartNewGameVisibility = Visibility.Hidden;
                });
            }
        }        
        
        public ICommand GenerateNameCommand
        {
            get
            {
                return new UiCommand(o =>
                {
                    NewGameName = NameGenerator.NameGenerator.Generate();
                });
            }
        }

        public ICommand StartNewGameCommand
        {
            get
            {
                return new UiCommand(o =>
                {
                    var gameState = GameStateFactory.Crate();
                    SaveHelper.Save(gameState, NewGameName);
                    OnGameStart(new MainManager(NewGameName, gameState));
                });
            }
        }

        public ICommand LoadGameCommand
        {
            get
            {
                return new UiCommand(o =>
                {
                    var gameState = SaveHelper.Load(SavedGames[SelectedGameToLoad]);
                    OnGameStart(new MainManager(SavedGames[SelectedGameToLoad], gameState));
                });
            }
        }

    }
}
