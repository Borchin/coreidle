﻿using System;
using System.Windows;
using System.Windows.Input;
using JetBrains.Annotations;

namespace CoreIdle.Ui
{
    internal sealed class StatefulButtonViewModel : ViewModelBase
    {
        [NotNull] private readonly Action _execute;

        private string _tooltip;
        public string Tooltip
        {
            get => _tooltip;
            set
            {
                if (_tooltip == value) return;
                _tooltip = value;
                OnPropertyChanged(() => Tooltip);
            }
        }

        private Visibility _mainButtonVisibility;
        public Visibility MainButtonVisibility
        {
            get => _mainButtonVisibility;
            set
            {
                if (_mainButtonVisibility == value) return;
                _mainButtonVisibility = value;
                OnPropertyChanged(() => MainButtonVisibility);
            }
        }

        private bool _isEnabled;
        internal bool IsEnabled
        {
            get => _isEnabled;
            set
            {
                MainButtonVisibility = value ? Visibility.Visible : Visibility.Hidden;
                _isEnabled = value;
            }
        }

        internal StatefulButtonViewModel(string tooltip, [NotNull] Action execute)
        {
            if(string.IsNullOrWhiteSpace(tooltip)) throw new ArgumentNullException(nameof(tooltip));
            _execute = execute ?? throw new ArgumentNullException(nameof(execute));
            Tooltip = tooltip;
        }

        public ICommand ExecuteCommand
        {
            get
            {
                return new UiCommand(o => _execute());
            }
        }
    }
}
