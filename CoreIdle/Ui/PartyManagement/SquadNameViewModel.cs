﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Windows;
using System.Windows.Input;
using CoreIdle.Character;

namespace CoreIdle.Ui.PartyManagement
{
    internal sealed class SquadNameViewModel : ViewModelBase
    {
        [NotNull] private readonly Squad _squad;

        private Visibility _renameVisibility = Visibility.Hidden;
        public Visibility RenameVisibility
        {
            get => _renameVisibility;
            set
            {
                if (_renameVisibility == value) return;
                _renameVisibility = value;
                OnPropertyChanged(() => RenameVisibility);
            }
        }

        private string _currentName;
        public string CurrentName
        {
            get => _currentName;
            set
            {
                if (_currentName == value) return;
                _currentName = value;
                OnPropertyChanged(() => CurrentName);
            }
        }

        private string _renameTo;
        public string RenameTo
        {
            get => _renameTo;
            set
            {
                if (_renameTo == value) return;
                _renameTo = value;
                OnPropertyChanged(() => RenameTo);
            }
        }

        private bool _renameIsEnabled = true;
        public bool RenameIsEnabled
        {
            get => _renameIsEnabled;
            set
            {
                if (_renameIsEnabled == value) return;
                _renameIsEnabled = value;
                OnPropertyChanged(() => RenameIsEnabled);
            }
        }

        internal SquadNameViewModel([NotNull] Squad squad)
        {
            _squad = squad ?? throw new ArgumentNullException(nameof(squad));
            CurrentName = squad.Name;
        }

        public ICommand RenameCommand
        {
            get
            {
                return new UiCommand(o =>
                {
                    RenameIsEnabled = false;
                    RenameVisibility = Visibility.Visible;
                    RenameTo = CurrentName;
                });
            }
        }

        public ICommand SaveCommand
        {
            get
            {
                return new UiCommand(o =>
                {
                    RenameIsEnabled = true;
                    RenameVisibility = Visibility.Hidden;
                    _squad.Name = RenameTo;
                    CurrentName = RenameTo;
                });
            }
        }

        public ICommand CancelCommand
        {
            get
            {
                return new UiCommand(o =>
                {
                    RenameIsEnabled = true;
                    RenameVisibility = Visibility.Hidden;
                });
            }
        }
    }
}
