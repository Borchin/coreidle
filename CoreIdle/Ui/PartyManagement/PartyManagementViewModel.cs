﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace CoreIdle.Ui.PartyManagement
{
    internal sealed class PartyManagementViewModel : ContentPageBase
    {
        [NotNull] private readonly MainManager _mainManager;

        [NotNull] public SquadNameViewModel SquadNameViewModel { get; }

        internal PartyManagementViewModel(string title, [NotNull] MainManager mainManager) : base(title)
        {
            _mainManager = mainManager ?? throw new ArgumentNullException(nameof(mainManager));
            SquadNameViewModel = new SquadNameViewModel(mainManager.CurrentGameState.GameServer.PlayerSquad);
        }
    }
}
