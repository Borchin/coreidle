﻿using System;
using System.Windows;

namespace CoreIdle.Ui
{
    /// <summary>
    /// Interaction logic for StatefulButton.xaml
    /// </summary>
    public partial class StatefulButton
    {
        public static string GetSource(DependencyObject depObject)
        {
            if(depObject == null) throw new ArgumentNullException(nameof(depObject));

            return (string)depObject.GetValue(SourceProperty);
        }

        public static void SetSource(DependencyObject depObject, string value)
        {
            if (depObject == null) throw new ArgumentNullException(nameof(depObject));

            depObject.SetValue(SourceProperty, value);
        }

        private static void OnSourceChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            var svgControl = ((StatefulButton)obj).MainButton;
            if (svgControl != null)
            {
                svgControl.Source = new Uri((string)e.NewValue, UriKind.Relative);
            }
        }

        public static readonly DependencyProperty SourceProperty =
            DependencyProperty.RegisterAttached("Source",
                typeof(string), typeof(StatefulButton),
                new PropertyMetadata(null, OnSourceChanged));

        public StatefulButton()
        {
            InitializeComponent();
        }
    }
}
