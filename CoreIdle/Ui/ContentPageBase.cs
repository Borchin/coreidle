﻿namespace CoreIdle.Ui
{
    internal abstract class ContentPageBase : ViewModelBase
    {
        public string Title { get; }

        public ContentPageBase(string title)
        {
            Title = title;
        }
    }
}
