﻿using System;
using JetBrains.Annotations;

namespace CoreIdle.Ui
{
    internal enum PageSwitch
    {
        GlobalUpgrades,
        NewServer,
        PartyManage,
        Adventure
    }

    internal sealed class MainMenuViewModel : ViewModelBase
    {
        internal event EventHandler<PageSwitch> SwitchPage;
        private void OnSwitchPage(PageSwitch e)
        {
            SwitchPage?.Invoke(this, e);
        }

        [NotNull] private readonly MainManager _mainManager;

        [NotNull] public StatefulButtonViewModel GlobalUpgradesButton { get; }
        [NotNull] public StatefulButtonViewModel NewServerButton { get; }
        [NotNull] public StatefulButtonViewModel PartyButton { get; }
        [NotNull] public StatefulButtonViewModel AdventureButton { get; }

        internal MainMenuViewModel([NotNull] MainManager mainManager)
        {
            _mainManager = mainManager ?? throw new ArgumentNullException(nameof(mainManager));
            _mainManager.FeatureChanged += (sender, args) => ChangeFeatures();

            NewServerButton = new StatefulButtonViewModel("Select new server", () => OnSwitchPage(PageSwitch.NewServer));
            GlobalUpgradesButton = new StatefulButtonViewModel("Global upgrades", () => OnSwitchPage(PageSwitch.GlobalUpgrades));
            PartyButton = new StatefulButtonViewModel("Manage characters", () => OnSwitchPage(PageSwitch.PartyManage));
            AdventureButton = new StatefulButtonViewModel("Adventure", () => OnSwitchPage(PageSwitch.Adventure));

            ChangeFeatures();
        }

        private void ChangeFeatures()
        {
            var features = _mainManager.CurrentGameState.Features;
            GlobalUpgradesButton.IsEnabled = features.GlobalUpgrades;
            NewServerButton.IsEnabled = features.SelectNewServer;
            PartyButton.IsEnabled = features.ManageParty;
            AdventureButton.IsEnabled = features.ManageAdventures;
        }
    }
}
