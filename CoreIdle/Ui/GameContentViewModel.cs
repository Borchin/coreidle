﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using CoreIdle.Ui.Adventures;
using CoreIdle.Ui.PartyManagement;

namespace CoreIdle.Ui
{
    internal sealed class GameContentViewModel : ViewModelBase
    {
        [NotNull] private readonly Dictionary<PageSwitch, ContentPageBase> _contentPages = new Dictionary<PageSwitch, ContentPageBase>();

        [NotNull] private readonly MainManager _mainManager;

        [NotNull] public MainMenuViewModel MainMenuViewModel { get; }
        [NotNull] public CurrencyViewModel CurrencyViewModel { get; }
        [NotNull] public GameTimeViewModel GameTimeViewModel { get; }

        private ContentPageBase _currentContent;
        public ContentPageBase CurrentContent
        {
            get => _currentContent;
            set
            {
                if (_currentContent == value) return;
                _currentContent = value;
                OnPropertyChanged(() => CurrentContent);
            }
        }

        internal GameContentViewModel([NotNull] MainManager mainManager)
        {
            _mainManager = mainManager ?? throw new ArgumentNullException(nameof(mainManager));

            MainMenuViewModel = new MainMenuViewModel(mainManager);
            MainMenuViewModel.SwitchPage += (sender, page) => SwitchPage(page);

            CurrencyViewModel = new CurrencyViewModel(mainManager);
            GameTimeViewModel = new GameTimeViewModel(mainManager);
            
            SwitchPage(PageSwitch.GlobalUpgrades);
        }

        private void SwitchPage(PageSwitch page)
        {
            if (!_contentPages.ContainsKey(page))
            {
                switch (page)
                {
                    case PageSwitch.GlobalUpgrades:
                        _contentPages.Add(page, new GlobalUpgradesViewModel("Global upgrades"));
                        break;
                    case PageSwitch.NewServer:
                        _contentPages.Add(page, new SelectNewServerViewModel("Server management", _mainManager));
                        break;
                    case PageSwitch.PartyManage:
                        _contentPages.Add(page, new PartyManagementViewModel("Party management", _mainManager));
                        break;
                    case PageSwitch.Adventure:
                        _contentPages.Add(page, new AdventureManagementViewModel("Adventures"));
                        break;
                    default:
                        throw new ArgumentOutOfRangeException(nameof(page), page, @"Unexpected page value");
                }
            }

            CurrentContent = _contentPages[page];
        }
    }
}
