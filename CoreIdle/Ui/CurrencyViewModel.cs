﻿using System;
using JetBrains.Annotations;

namespace CoreIdle.Ui
{
    internal sealed class CurrencyViewModel : ViewModelBase
    {
        [NotNull] private readonly MainManager _mainManager;
        [NotNull] private readonly DeltaProcessor _test = new DeltaProcessor(TimeSpan.FromSeconds(1));

        private int _realMoney;
        public int RealMoney
        {
            get => _realMoney;
            set
            {
                if (_realMoney == value) return;
                _realMoney = value;
                OnPropertyChanged(() => RealMoney);
            }
        }

        internal CurrencyViewModel([NotNull] MainManager mainManager)
        {
            _mainManager = mainManager ?? throw new ArgumentNullException(nameof(mainManager));
            _mainManager.GameLoopManager.Update += OnUpdate;
        }

        private void OnUpdate(object sender, TimeSpan e)
        {
            if(_test.Check(e))
                RealMoney = ++_mainManager.CurrentGameState.Currency[CurrencyType.RealMoney];
        }
    }
}
