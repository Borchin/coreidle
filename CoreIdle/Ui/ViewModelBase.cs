﻿using System;
using System.ComponentModel;
using System.Linq.Expressions;
using System.Windows;

namespace CoreIdle.Ui
{
    /// <inheritdoc />
    /// <summary> INotifyPropertyChanged base implementation </summary>
    public class ViewModelBase : INotifyPropertyChanged
    {
        /// <inheritdoc />
        public event PropertyChangedEventHandler PropertyChanged;

        private static bool? _isInDesignMode;

        /// <summary>
        /// Calls PropertyChanged event with correct property name
        /// </summary>
        /// <example> OnPropertyChanged(() => Property); </example>
        /// <typeparam name="T"></typeparam>
        /// <param name="property"> Property as a lambda </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures"),
            System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1011:ConsiderPassingBaseTypesAsParameters")]
        protected void OnPropertyChanged<T>(Expression<Func<T>> property)
        {
            if (property == null) throw new ArgumentNullException(nameof(property));
            if (!(property.Body is MemberExpression expression))
                throw new NotSupportedException("Invalid expression passed. Only property member should be selected.");

            OnPropertyChanged(expression.Member.Name);
        }

        /// <summary>
        /// Tells the WPF binding system to re-evaluate all the bindings of that object
        /// </summary>
        protected void UpdateAllBindings()
        {
            OnPropertyChanged(string.Empty);
        }

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        /// <summary>
        /// Gets a value indicating whether the control is in design mode (running in Blend
        /// or Visual Studio).
        /// </summary>
        protected static bool IsInDesignMode
        {
            get
            {
                if (!_isInDesignMode.HasValue)
                {
#if SILVERLIGHT
                    _isInDesignMode = DesignerProperties.IsInDesignTool;
#else
                    DependencyProperty prop = DesignerProperties.IsInDesignModeProperty;
                    _isInDesignMode = (bool)DependencyPropertyDescriptor
                                     .FromProperty(prop, typeof(FrameworkElement))
                                     .Metadata.DefaultValue;
#endif
                }

                return _isInDesignMode.Value;
            }
        }
    }
}
