﻿using System.Collections.Generic;
using System.Linq;

namespace CoreIdle.NameGenerator
{
    internal static class NamePatterns
    {
        internal static Dictionary<string, string> LetterConversion { get; } = new Dictionary<string, string>
        {
            {"i", "1"},
            {"l", "1"},
            {"e", "3"},
            {"t", "7"},
            {"o", "0"},
            {"ch", "4"},
        };

        internal static List<string> RealName { get; } = new List<string>
        {
            "Artyom", "Tyoma", "Aleksandr", "Sasha", "Sanya", "Roman", "Roma", "Yevgeny", "Zhenya",
            "Ivan", "Vanya", "Maksim", "Maxim", "Max", "Denis", "Deniska", "Alexey", "Lyosha",
            "Dmitry", "Dima", "Danyl", "Danya", "Sergey", "Seryozha", "Nikolai", "Kolya", "Kolyan",
            "Konstantin", "Kostya", "Nikita", "Mikhail", "Misha", "Muxacb", "Boris", "Borya", "Victor",
            "Vitya", "Gennady", "Gyena", "Gena", "Vyacheslav", "Slava", "Vladimir", "Vova", "Vovan",
            "Volodya", "Andrey", "Andrusha", "Anatoly", "Tolya", "Tolyan", "Ilya", "Ilyusha",
            "Kirill", "Kirusha", "Oleg", "Olezhek"
        };

        internal static List<string> Nickname { get; } = new List<string>
        {
            //part1
            "Aspect", "Kraken", "Bender", "Lynch", "BigPapa", "MadDog", "Bowser", "ODoyle", "Bruise", "Psycho", "Cannon",
            "Ranger", "Clink", "Ratchet", "Cobra", "Reaper", "Colt", "Rigs", "Crank", "Ripley", "Creep", "Roadkill",
            "Daemon", "Ronin", "Decay", "Rubble", "Diablo", "Sasquatch", "Doom", "Scar", "Dracula", "Shiver", "Dragon",
            "Skinner", "Fender", "SkullCrusher", "Fester", "Slasher", "Fisheye", "Steelshot", "Flack", "Surge", "Gargoyle", "Sythe",
            "Grave", "Trip", "Gunner", "Trooper", "Hash", "Tweek", "Hashtag", "Vein", "Indominus", "Void", "Ironclad",
            "Wardon", "Killer", "Wraith", "Knuckles", "Zero", "Steel", "Kevlar", "Lightning", "Tito", "BulletProof", "FireBred",
            "Titanium", "Hurricane", "Ironsides", "IronCut", "Tempest", "IronHeart", "SteelForge", "Pursuit", "SteelFoil", "Uprising", "Overthrow",
            "Breaker", "Sabotage", "Dissent", "Subversion", "Rebellion", "Insurgent", "Loch", "Golem", "Wendigo", "Rex", "Hydra",
            "Behemoth", "Balrog", "Manticore", "Gorgon", "Basilisk", "Minotaur", "Leviathan", "Cerberus", "Mothman", "Sylla", "Charybdis",
            "Orthros", "Baal", "Cyclops", "Satyr", "Azrael", "Ballistic", "Furor", "Uproar", "Fury", "Ire", "Demented",
            "Wrath", "Madness", "Schizo", "Rage", "Savage", "Manic", "Frenzy", "Mania", "Derange", "V", "Atilla",
            "Darko", "Terminator", "Conqueror", "MadMax", "Siddhartha", "Suleiman", "BillytheButcher", "Thor", "Napoleon", "Maximus", "Khan",
            "Geronimo", "Leon", "Leonidas", "Dutch", "Cyrus", "Hannibal", "Dux", "MrBlonde", "Agrippa", "JesseJames", "Matrix",
            "Bleed", "XSkull", "Gut", "Nail", "Jawbone", "Socket", "Fist", "Skeleton", "Footslam", "Tooth", "Craniax",
            "HeadKnocker", "K9", "Bone", "Razor", "Kneecap", "Cut", "Slaughter", "Soleus", "Gash", "Scalp", "Blood",
            "Scab", "Torque", "Wracker", "Annihilator", "Finisher", "Wrecker", "Destroyer", "Overtaker", "Clencher", "Stabber", "Saboteur",
            "Masher", "Hitter", "Rebel", "Crusher", "Obliterator", "Eliminator", "Slammer", "Exterminator", "HellRaiser", "Thrasher", "Ruiner",
            "Mutant", "Torpedo", "Wildcat", "Automatic", "Cannon", "Hellcat", "Glock", "Mortar", "Tomcat", "Sniper", "Siege",
            "Panther", "Carbine", "Bullet", "Jaguar", "Javelin", "Aero", "Bomber", "Howitzer", "Albatross", "StrikeEagle", "Gatling",
            "Arsenal", "Rimfire", "Avenger", "Hornet", "Centerfire", "Hazzard", "Demolition", "PowerTrain", "Yarder", "Chainsaw", "Excavator",
            "Trencher", "Wrench", "Shovel", "PileDriver", "Terror", "Demise", "Phantom", "Freak", "Grim", "Sepulcher", "Axe",
            "Menace", "Damned", "Axeman", "Dementor", "Kafka", "Executioner", "Nightshade", "Phantasm", "Hollowman", "Venom", "Scream",
            "Garrot", "TheUnholy", "Shriek", "Abyss", "Rot", "Wraith", "Chasm", "Omen", "Bodybag", "Ghoul", "Midnight",
            "Morgue", "Mace", "Falchion", "Montante", "Battleaxe", "Zweihander", "Hatchet", "Billhook", "Club", "Hammer", "Caltrop",
            "Maul", "Sledgehammer", "Longbow", "Bludgeon", "Harpoon", "Crossbow", "Lance", "Angon", "Pike", "TigerClaw", "FireLance",
            "Poleaxe", "BrassKnuckle", "Matchlock", "Quarterstaff", "Gauntlet", "Bullwhip", "WarHammer", "Katar", "FlyingClaw", "Spear", "Dagger",
            "Slungshot", "Katana", "Gladius", "Aspis", "Saber", "Cutlass", "Blade", "Broadsword", "Scimitar", "Lockback", "Claymore",
            "Espada", "Machete", "Grizzly", "Wolverine", "Deathstalker", "Snake", "Wolf", "Scorpion", "Vulture", "Claw", "Boomslang",
            "Falcon", "Fang", "Viper", "Ram", "Grip", "Sting", "Boar", "BlackMamba", "Lash", "Tusk", "Goshawk",
            "Gnaw", "Amazon", "Majesty", "Anomoly", "Malice", "Banshee", "Mannequin", "Belladonna", "Minx", "Beretta", "Mirage",
            "BlackBeauty", "Nightmare", "Calypso", "Nova", "Carbon", "Pumps", "Cascade", "Raven", "Colada", "Resin", "Cosma",
            "Riveter", "Cougar", "Rogue", "Countess", "Roulette", "Enchantress", "Shadow", "Enigma", "Siren", "FemmeFatale", "Stiletto",
            "Firecracker", "Tattoo", "Geisha", "TBack", "Goddess", "Temperance", "HalfPint", "Tequila", "Harlem", "Terror", "Heroin",
            "Thunderbird", "Infinity", "Ultra", "Insomnia", "Vanity", "Ivy", "Velvet", "Legacy", "Vixen", "Lithium", "Voodoo",
            "Lolita", "Wicked", "Lotus", "Widow", "Mademoiselle", "Xenon", "Kahina", "Teuta", "Isis", "Dihya", "Artemis",
            "Nefertiti", "RunningEagle", "Atalanta", "Sekhmet", "Colestah", "Athena", "Ishtar", "CalamityJane", "Enyo", "Ashtart", "PearlHeart",
            "Bellona", "Juno", "BelleStarr", "WhiteTights", "Tanit", "HuaMulan", "Shieldmaiden", "Devi", "Boudica", "Valkyrie", "Selkie",
            "Medb", "Cleo", "Venus", "Madam", "Empress", "Marquess", "Duchess", "Baroness", "Herzogin", "Fate", "Beguile",
            "Deviant", "Illusion", "Crafty", "Variance", "Delusion", "Deceit", "Caprice", "Deception", "Waylay", "Aberr", "Myth",
            "Ambush", "Variant", "Daydream", "Feint", "Hero", "NightTerror", "Catch22", "Villain", "Figment", "Puzzler", "Daredevil",
            "Virtual", "Curio", "Mercenary", "Chicanery", "Prodigy", "Voyager", "Trick", "Breach", "Wanderer", "Vile", "MissFortune",
            "Audacity", "Horror", "Vex", "Swagger", "Dismay", "Grudge", "Nerve", "Phobia", "Enmity", "Egomania", "Fright",
            "Animus", "Scheme", "Panic", "Hostility", "Paramour", "Agony", "Rancor", "Xhibit", "Inferno", "Malevolence", "Charade",
            "Blaze", "Poison", "Hauteur", "Crucible", "Spite", "Vainglory", "Haunter", "Spitefulness", "Narcissus", "Bane", "Venom",
            "Brass", "Camden", "Baltimore", "CrownHeights", "Detroit", "LA", "DirtyDirty", "McKinley", "NYC", "ATL", "Fiend",
            "Spirit", "Spellbinder", "Goblin", "Kelpie", "Jezebel", "Oracle", "Vamp", "Sorceress", "Soul", "Temptress", "SheDevil",
            "Revenant", "Diviner", "Hellcat", "Poltergeist", "Exorcist", "SheWolf", "Zombie", "Seer", "Madcap", "Armor", "Blaser",
            "Savage", "Benelli", "Glock", "Seraphim", "Remington", "Ruger", "Winchester", "Aeon", "Tank", "Hawkeye", "Kiddo",
            "Torchy", "Medusa", "Buffy", "Trinity", "Irons", "Coffy", "Zoe", "Storm", "Eowyn", "Zen", "Jubilee",
            "Croft", "Alyx", "Dazzler", "Leeloo", "Katniss", "Aeryn", "Mathilda", "Linh", "Arya", "Padme", "Polgara",
            "Ygritte", "Ramona", "Elektra", "Bayonetta", "SilkSpectre", "Catwoman", "Sindel", "Helium", "Mercury", "Entropy", "Beryllium",
            "Radon", "Radioactive", "Neon", "Radium", "Radiate", "Phosphorus", "Element", "Ion", "Phosphorescent", "Elemental", "Eon",
            "Illumine", "LabRat", "Photon", "Chromium", "Acid", "Redox", "Arsenic", "Atom", "Redux", "Zinc", "Electron",
            "HotSalt", "Selenium", "Atomic", "Vapor", "Xenon", "Nuclear", "Volt", "Osmium", "EarthMetal", "XRay", "Fox",
            "Nightshade", "Immortal", "Coyote", "Mercury", "Meteor", "Crow", "Cyanide", "Comet", "Spider", "Toxin", "Protostar",
            "Bat", "Virus", "Medium", "Serpent", "Arsenic", "DarkMatter", "Satyr", "Orb", "Hypernova", "Oleander", "Astor",
            "Stratosphere", "Hemlock", "Mortal", "Supernova", "Vegas", "SnakeEyes", "Militant", "Blackjack", "HighRoller", "Lock", "Baccarat",
            "PocketRocket", "TheMoney", "RedDog", "Mojave", "Dice", "Poker", "Bellagio", "Rocks", "Keno", "Nevada", "Watchface",
            "TheHouse", "Reno", "BigTime", "Bookie", "Steppe", "TwoFace", "Arbitrage", "Fishnet", "DeepPockets", "DoubleDouble", "Dagger",
            "RealDeal", "Explosive", "Grenade", "Pyro", "BlackCat", "RomanCandle", "Gunpowder", "M80", "BabylonCandle", "Mortar", "SmokeBomb",
            "Missile", "Firebringer", "Amaretto", "Dom", "Noir", "Bacardi", "Don", "Scotch", "Cognac", "Joose", "Whiskey",
            "Forever", "Revolution", "Berserk", "Unstoppable", "Sever", "Bitten", "Eternity", "Diehard", "Corybantic", "Fanatic", "Zealot",
            "Crazed", "XTreme", "Alien", "Delirious", "Rabid", "Predator", "Agitator", "Radical", "Barbarian", "Maniac",
            //part2
            "Sailor", "Rockstar", "Loco", "CrazyEyes", "Devil", "Pitbull", "Warrior", "Snapper", "BadBoy", "KnockOut", "KO",
            "Daredevil", "Frostbite", "Ghost", "Damned", "Grimm", "Pirate", "Obliterator", "Nasty", "Viking", "Terror", "Punk",
            "Maverick", "Dementor", "Militant", "Vegas", "SnakeEyes", "Blackjack", "HighRoller", "Hollowman", "Rocks", "Nevada", "Nightshade",
            "Reno", "TwoFace", "Eclipse", "Scream", "HellRaiser", "Rocker", "Executioner", "Maniac", "Radical", "Crazed", "XTreme",
            "Barbarian", "Flame", "Flamethrower", "CoolHandLuke", "TheAxeMurderer", "PocketRocket", "RedDog", "Mojave", "Dice", "Poker", "TheCrippler",
            "Rampage", "TheDeanofMean", "Delirious", "Predator", "Rubble", "Mayhem", "Nightmare", "TheOutlaw", "TheNaturalBornKiller", "Creep", "MadDog",
            "Psycho", "Bacardi", "Scotch", "HeadKnocker", "Bruise", "Crank", "Reaper", "Hazzard", "Clink", "Lynch", "Knuckles",
            "Daemon", "Diablo", "Doom", "Scar", "Cut", "Decay", "Roadkill", "TheUnholy", "Abyss", "Rot", "Ghoul",
            "Midnight", "Shiver", "Gargoyle", "Killer", "Wraith", "Grave", "Berserker", "Dissent", "Subversion", "Rebellion", "Insurgent",
            "Upsurge", "Uprising", "Overthrow", "Breaker", "Sabotage", "Reaper", "Mothman", "Fury", "Wrath", "Madness", "Schizo",
            "Rage", "Freak", "Savage", "Manic", "Frenzy", "Mania", "MadMax", "BillytheButcher", "XSkull", "Gut", "Nail",
            "Jawbone", "Razor", "Crazy",
            //real nicknames
            "DVP", "kaganpwnz", "Squeez", "progamer", "ArtOf", "AlexQuattro",
            "DanielDefo", "Oner", "Higgs", "Ershess", "Satyricon",
        };

        internal static List<string> StupidPart { get; } = new List<string>
        {
            "Slayer", "Killer", "Ubica", "Ebasher", "Pulya", "Topchik", "Zoomer", "Konch", "Gnida", "Soska", "Doomer", "Kopatich",
            "Zaloopa", "DaiDeneg", "YaPosral", "Sperma", "Mocha", "Skilovik", "Cock", "Petushara", "Pizdos", "Strelok", "Pidrila", 
            "Govno", "Barebuh", "Gonzolik", "CMEPTb", "ADIDAS", "PUTIN", "Putin", "Amerikos", "DNR", "SlavaUkraine", "Bandera",
            "Zapor", "Nakatim", "ZaDedov", "Ztochka", "Pizda", "Penis", "Eldak", "Baleba", "Pugacheva", "KalKalich", "Sex",
            "Gnomik", "Drochila", "SoloMid", "Feeder", "YaLivaiu", "SinShalavi", "Telega"
        };

        internal static List<string> Brackets { get; } = new List<string>
        {
            "xxx", "zzz", "zxz", "xzx", "ooo", "x", "xx", "z", "zz", "oo", "oOo",
            "xXx", "XXX", "X", "o0o", "www", "q"
        };

        internal static List<string> Prefix { get; } = new List<string>
        {
            "tupa", "pro100", "GG", "chisto", "I", "PR0100", "Top"
        }.Concat(Brackets).ToList();


        internal static List<string> Postfix { get; } = new List<string>
        {
            "228", "QQ", "gg", "TOP", "PodGovnom", "NaButilke", "Kek", "LOL", "Ru", "DNR", "LNR"
        }.Concat(RegionPostfixes()).ToList();

        private static IEnumerable<string> RegionPostfixes()
        {
            for (var i = 0; i < 100; i++)
            {
                yield return $"{i:D2}RUS";
                yield return $"{i:D2}Rus";
                yield return $"{i:D2}rus";
            }
        }
    }
}
