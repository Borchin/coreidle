﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;

namespace CoreIdle.NameGenerator
{
    public static class NameGenerator
    {
        private static readonly Random Random = new Random(Guid.NewGuid().GetHashCode());
        private const int MaxNameLength = 20;

        public static string Generate()
        {
            //Base name
            var useRealName = BoolRandom(25);
            var name = GetRandom(useRealName ? NamePatterns.RealName : NamePatterns.Nickname);
            var spaceLeft = MaxNameLength - name.Length;

            //Use second nickname with base
            var useSecondNick = BoolRandom(useRealName ? 75 : 35);
            if (useSecondNick)
            {
                var useStupidName = BoolRandom(30);

                var usableNicks = useStupidName 
                    ? NamePatterns.StupidPart.Where(n => n.Length < spaceLeft).ToList()
                    : NamePatterns.Nickname.Where(n => n.Length < spaceLeft).ToList();

                if (usableNicks.Count > 0)
                {
                    var secondNick = GetRandom(usableNicks);
                    var useSecondAfterFirst = BoolRandom(50);
                    name = useSecondAfterFirst ? $"{name}{secondNick}" : $"{secondNick}{name}";
                    spaceLeft = MaxNameLength - name.Length;
                }
            }

            //Prefix
            var usePrefix = BoolRandom(10 + spaceLeft * 2);
            if (usePrefix)
            {
                var prefixThatCanBeUsed = NamePatterns.Prefix.Where(n => n.Length < spaceLeft).ToList();
                if (prefixThatCanBeUsed.Count > 0)
                {
                    name = $"{GetRandom(prefixThatCanBeUsed)}{name}";
                    spaceLeft = MaxNameLength - name.Length;
                }
            }

            //Postfix
            var usePostfix = BoolRandom(10 + spaceLeft * 2);
            if (usePostfix)
            {
                var postfixThatCanBeUsed = NamePatterns.Postfix.Where(n => n.Length < spaceLeft).ToList();
                if (postfixThatCanBeUsed.Count > 0)
                {
                    name = $"{name}{GetRandom(postfixThatCanBeUsed)}";
                    spaceLeft = MaxNameLength - name.Length;
                }
            }

            //Brackets
            var useBrackets = !usePrefix && !usePostfix && BoolRandom(25);
            if (useBrackets)
            {
                var bracketsThatCanBeUsed = NamePatterns.Brackets.Where(n => n.Length * 2 < spaceLeft).ToList();
                if (bracketsThatCanBeUsed.Count > 0)
                {
                    var bracket = GetRandom(bracketsThatCanBeUsed);
                    name = $"{bracket}{name}{bracket}";
                    spaceLeft = MaxNameLength - name.Length;
                }
            }

            name = MakeDigitName(name);

            //Make short name end with digit. If brackets or postfix was added, or name ends with digit we skip this
            var canAddNumber = !useBrackets && !usePostfix && spaceLeft > MaxNameLength / 2 && !char.IsDigit(name.Last());
            if (canAddNumber)
            {
                if (BoolRandom(10))
                {
                    name = $"{name}{Random.Next(1, 3)}";
                    spaceLeft = MaxNameLength - name.Length;
                }
                else if (BoolRandom(5))
                {
                    name = $"{name}{Random.Next(4, 74)}";
                    spaceLeft = MaxNameLength - name.Length;
                }
                else if (BoolRandom(15))
                {
                    name = $"{name}{Random.Next(1975, 2020)}";
                    spaceLeft = MaxNameLength - name.Length;
                }
                else if (BoolRandom(15))
                {
                    name = $"{name}{Random.Next(75, 100)}";
                    spaceLeft = MaxNameLength - name.Length;
                }
                else if (name.Length < 5)
                {
                    name = $"{name}{Random.Next(99, 999)}";
                    spaceLeft = MaxNameLength - name.Length;
                }
            }

            if (BoolRandom(7))
                name = name.ToUpper(CultureInfo.InvariantCulture);

            return name;
        }

        private static string MakeDigitName(string name)
        {
            foreach (var conversion in NamePatterns.LetterConversion)
            {
                var allIndexes = AllIndexesOf(name, conversion.Key);

                if (allIndexes.Length > 0)
                {
                    var replaceOne = BoolRandom(7);
                    var replaceAll = !replaceOne && BoolRandom(5);

                    if (replaceAll)
                        name = name.Replace(conversion.Key, conversion.Value, StringComparison.InvariantCulture);

                    if (replaceOne)
                    {
                        var indexToReplace = allIndexes[Random.Next(0, allIndexes.Length)];
                        name = name.Remove(indexToReplace, conversion.Key.Length);
                        name = name.Insert(indexToReplace, conversion.Value);
                    }
                }
            }

            return name;
        }

        private static int[] AllIndexesOf(string str, string substr)
        {
            if (string.IsNullOrWhiteSpace(str) ||
                string.IsNullOrWhiteSpace(substr))
            {
                throw new ArgumentException("String or substring is not specified.");
            }

            var indexes = new List<int>();
            var index = 0;

            while ((index = str.IndexOf(substr, index, StringComparison.OrdinalIgnoreCase)) != -1)
            {
                indexes.Add(index++);
            }

            return indexes.ToArray();
        }

        private static string GetRandom([NotNull] List<string> source)
        {
            return source[Random.Next(0, source.Count)];
        }

        private static bool BoolRandom(int chance)
        {
            return Random.Next(0, 100) <= chance;
        }
    }
}
