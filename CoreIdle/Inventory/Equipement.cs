﻿using System;
using System.Collections.Generic;
using System.Linq;
using CoreIdle.Character;

namespace CoreIdle.Inventory
{
    internal abstract class Equipement : IEquipment
    {
        protected Equipement(string itemClassName)
        {
            ItemClassName = itemClassName;
        }

        public Guid GuidKey { get; set; }
        public string Name { get; set; }
        public ItemId Id { get; set; }
        public int BasePrice { get; set; }
        public string ItemClassName { get; }
        public EquipmentGrade Grade { get; set; }
        public List<StatChange> StatChanges { get; set; } = new List<StatChange>();
        public IEnchantInfo EnchantInfo { get; set; }

        public List<StatChange> SummaryStats => EnchantInfo == Inventory.EnchantInfo.EnchantNotSupported
            ? StatChanges
            : StatChanges.Concat(EnchantInfo.EnchantBonuses).ToList();

        public BaseItem ToBaseItem() =>
            new BaseItem
            {
                Id = Id,
                EnchantLevel = EnchantInfo == Inventory.EnchantInfo.EnchantNotSupported ? 0 : EnchantInfo.EnchantLevel,
                GuidKey = GuidKey
            };

        public override string ToString()
        {
            return $"Guid:{GuidKey} Name:{Name} Id:{Id}\n BasePrice:{BasePrice} ItemClassName:{ItemClassName} Grade:{Grade}\n {EnchantInfo}";
        }
    }
}
