﻿using System;
using System.Linq;
using CoreIdle.Character;

namespace CoreIdle.Inventory
{
    internal abstract class Weapon : Equipement, IWeapon
    {
        protected Weapon(HandUsage hand, int baseAttackSpeed, string itemClassName, int pAtk, int mAtk, int critChance) : base(itemClassName)
        {
            Hand = hand;
            BaseAttackSpeed = baseAttackSpeed;
            StatChanges.Add(new StatChange(Stat.Patk, StatCalcAction.AddToBase, pAtk));
            StatChanges.Add(new StatChange(Stat.Matk, StatCalcAction.AddToBase, mAtk));
            StatChanges.Add(new StatChange(Stat.PCritChance, StatCalcAction.AddToBase, critChance));
        }

        public HandUsage Hand { get; }
        public int BaseAttackSpeed { get; }
        public int Patk => Convert.ToInt32(SummaryStats.Where(s => s.Stat == Stat.Patk).Sum(s => s.Value));
        public int Matk => Convert.ToInt32(SummaryStats.Where(s => s.Stat == Stat.Matk).Sum(s => s.Value));

        public override string ToString()
        {
            return $"{base.ToString()}\n Hand:{Hand} BaseAttackSpeed:{BaseAttackSpeed} Patk:{Patk} Matk:{Matk}";
        }
    }

    internal sealed class OneHandBlunt : Weapon
    {
        public OneHandBlunt(int pAtk, int mAtk) : base(HandUsage.Right, ConstAttackSpeed.Fast, "One-handed blunt", pAtk, mAtk, 4) { }
    }

    internal sealed class TwoHandBlunt : Weapon
    {
        public TwoHandBlunt(int pAtk, int mAtk) : base(HandUsage.Both, ConstAttackSpeed.Normal, "Two-handed blunt", pAtk, mAtk, 4) { }
    }

    internal sealed class TwoHandSword : Weapon
    {
        public TwoHandSword(int pAtk, int mAtk) : base(HandUsage.Both, ConstAttackSpeed.Normal, "Two-handed sword", pAtk, mAtk, 8) { }
    }

    internal sealed class OneHandSword : Weapon
    {
        public OneHandSword(int pAtk, int mAtk) : base(HandUsage.Right, ConstAttackSpeed.Fast, "One-handed sword", pAtk, mAtk, 8) { }
    }

    internal sealed class Dagger : Weapon
    {
        public Dagger(int pAtk, int mAtk) : base(HandUsage.Right, ConstAttackSpeed.VeryFast, "Dagger", pAtk, mAtk, 12) { }
    }

    internal sealed class DualFist : Weapon
    {
        public DualFist(int pAtk, int mAtk) : base(HandUsage.Both, ConstAttackSpeed.Normal, "Dual fist / two-handed", pAtk, mAtk, 4) { }
    }

    internal sealed class DualSword : Weapon
    {
        public DualSword(int pAtk, int mAtk) : base(HandUsage.Both, ConstAttackSpeed.Normal, "Dual sword / two-handed", pAtk, mAtk, 8) { }
    }

    internal sealed class Bow : Weapon
    {
        public Bow(int pAtk, int mAtk) : base(HandUsage.Both, ConstAttackSpeed.Bow, "Bow / two-handed", pAtk, mAtk, 12) { }
    }

    internal sealed class Polearm : Weapon
    {
        public Polearm(int pAtk, int mAtk) : base(HandUsage.Both, ConstAttackSpeed.Normal, "Polearm / two-handed", pAtk, mAtk, 8) { }
    }
}
