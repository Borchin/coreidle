﻿using System;
using System.Linq;
using CoreIdle.Character;

namespace CoreIdle.Inventory
{
    internal sealed class BodyArmor : Equipement
    {
        internal BodyArmor(ArmorKind armorKind, int pDef) : base("Body armor")
        {
            ArmorKind = armorKind;
            StatChanges.Add(new StatChange(Stat.Pdef, StatCalcAction.AddToBase, pDef));
        }

        internal ArmorKind ArmorKind { get; }
        internal int Pdef => Convert.ToInt32(SummaryStats.Where(s => s.Stat == Stat.Pdef).Sum(s => s.Value));

        public override string ToString()
        {
            return $"{base.ToString()}\n ArmorKind:{ArmorKind} Pdef:{Pdef}";
        }
    }

    internal sealed class Gloves : Equipement
    {
        internal Gloves(int pDef) : base("Gloves")
        {
            StatChanges.Add(new StatChange(Stat.Pdef, StatCalcAction.AddToBase, pDef));
        }

        internal int Pdef => Convert.ToInt32(SummaryStats.Where(s => s.Stat == Stat.Pdef).Sum(s => s.Value));

        public override string ToString()
        {
            return $"{base.ToString()}\n Pdef:{Pdef}";
        }
    }

    internal sealed class Helmet : Equipement
    {
        internal Helmet(int pDef) : base("Helmet")
        {
            StatChanges.Add(new StatChange(Stat.Pdef, StatCalcAction.AddToBase, pDef));
        }

        internal int Pdef => Convert.ToInt32(SummaryStats.Where(s => s.Stat == Stat.Pdef).Sum(s => s.Value));

        public override string ToString()
        {
            return $"{base.ToString()}\n Pdef:{Pdef}";
        }
    }

    internal sealed class Boots : Equipement
    {
        internal Boots(int pDef) : base("Boots")
        {
            StatChanges.Add(new StatChange(Stat.Pdef, StatCalcAction.AddToBase, pDef));
        }

        internal int Pdef => Convert.ToInt32(SummaryStats.Where(s => s.Stat == Stat.Pdef).Sum(s => s.Value));

        public override string ToString()
        {
            return $"{base.ToString()}\n Pdef:{Pdef}";
        }
    }
}
