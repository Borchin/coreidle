﻿using System;
using System.Collections.Generic;
using CoreIdle.Character;

namespace CoreIdle.Inventory
{
    internal interface IEquipment : ISellable
    {
        Guid GuidKey { get; set; }
        string Name { get; set; }
        ItemId Id { get; set; }
        BaseItem ToBaseItem();
        string ItemClassName { get; }
        EquipmentGrade Grade { get; set; }
        List<StatChange> StatChanges { get; set; }
        IEnchantInfo EnchantInfo { get; set; }
        List<StatChange> SummaryStats { get; }
    }
}
