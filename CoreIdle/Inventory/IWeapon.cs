﻿namespace CoreIdle.Inventory
{
    internal interface IWeapon : IHandEquipment
    {
        int BaseAttackSpeed { get; }
        int Patk { get; }
        int Matk { get; }
    }
}
