﻿namespace CoreIdle.Inventory
{
    public enum EquipmentGrade
    {
        Ng = 0,
        D = 20,
        C = 40,
        B = 52,
        A = 61,
        S = 76
    }
    
    public enum ArmorKind
    {
        Magic,
        Light,
        Heavy
    }

    public enum HandUsage
    {
        Right,
        Left,
        Both
    }

    public enum InventorySlot
    {
        Helmet,
        Gloves,
        Boots,
        Body,
        RightHand,
        LeftHand,
        RightRing,
        LeftRing,
        LeftEarring,
        RightEarring,
        NecklaceEarring,
    }
}
