﻿using System;
using System.Collections.Generic;
using CoreIdle.Character;
using JetBrains.Annotations;

namespace CoreIdle.Inventory
{
    public enum ItemId
    {
        //1h blunt NG
        ApprenticeWand,
        ApprenticeRod,
        GuildMemberClub,
        Club,
        HeavyChisel,
        Mace,
        WandOfAdept,
        Sickle,
        SilversmithHammer,
        DwarvenMace,
        BoneClub,
        Buzdygan,
        IronHammer,
        //1h blunt D
        HandAxe,
        HeavyMace,


        //Dagger NG
        BoneDagger,
        //Dagger D
        AssassinKnife,


        //Bow NG
        ShortBow,
        //Bow D
        StrengthenedBow,

        //Dual swords D
        SaberXSaber,

        //Dual fist NG
        SpikedGloves,
        //Dual fist D
        BaghNakh,

        //Polearm NG
        ShortSpear,
        //Polearm D
        Trident,

        //1h Swords NG
        SquiresSword,
        ShortSword,
        //1h Sword D
        Saber,

        //2h Swords NG
        RedSunsetSword,
        //2h Swords D
        TwoHandedSword,

        //2h Blunt NG
        WillowStaff,
        //2h Blunt D
        BoneStaff,

        //Ring NG
        MagicRing,
        //Ring D
        RingOfDevotion,

        //Earring Ng
        ApprenticeEarring,
        //Earring D
        RedCrescentEarring,

        //Neck NG
        NecklaceOfMagic,
        //Neck D
        NecklaceOfDevotion,

        //Heavy body NG
        PieceBoneBreastplate,
        //Heavy body D
        RingMailBreastplate,

        //Light body NG
        SquireShirt,
        LeatherShirt,
        //Light body D
        PumaSkinShirt,

        //Magic body NG
        ApprenticeTunic,
        Tunic,
        //Magic body D
        CursedTunic,

        //Boots NG
        ClothShoes,
        //Boots D
        LeatherBoots,

        //Gloves NG
        ShortGloves,
        //Gloves D
        CraftedLeatherGloves,

        //Helmet NG
        ClothCap,
        //Helmet D
        BoneHelmet,
    }

    internal static class EquipmentFactory
    {
        private static readonly Dictionary<ItemId, Func<BaseItem, IEquipment>> Mapping = new Dictionary<ItemId, Func<BaseItem, IEquipment>>
        {
            //-------------------
            //1h blunt NG
            {ItemId.ApprenticeWand, baseItem => InitItem(new OneHandBlunt(5, 7), baseItem, "Apprentice's Wand", ConstPrice.Ng69, EquipmentGrade.Ng,0)},
            {ItemId.ApprenticeRod, baseItem => InitItem(new OneHandBlunt(6, 8), baseItem, "Apprentice's Rod", ConstPrice.Ng384, EquipmentGrade.Ng,0)},
            {ItemId.GuildMemberClub, baseItem => InitItem(new OneHandBlunt(6, 5), baseItem, "Guild Member's Club", ConstPrice.Ng69, EquipmentGrade.Ng,0)},
            {ItemId.Club, baseItem => InitItem(new OneHandBlunt(8, 6), baseItem, "Club", ConstPrice.Ng384, EquipmentGrade.Ng,0)},
            {ItemId.HeavyChisel, baseItem => InitItem(new OneHandBlunt(10, 8), baseItem, "Heavy Chisel", ConstPrice.Ng4035, EquipmentGrade.Ng,0)},
            {ItemId.Mace, baseItem => InitItem(new OneHandBlunt(11, 9), baseItem, "Mace", ConstPrice.Ng6250, EquipmentGrade.Ng,0)},
            {ItemId.WandOfAdept, baseItem => InitItem(new OneHandBlunt(11, 13), baseItem, "Wand Of Adept", ConstPrice.Ng12700, EquipmentGrade.Ng,0)},
            {ItemId.Sickle, baseItem => InitItem(new OneHandBlunt(12, 9), baseItem, "Sickle", ConstPrice.Ng9250, EquipmentGrade.Ng,0)},
            {ItemId.SilversmithHammer, baseItem => InitItem(new OneHandBlunt(13, 10), baseItem, "Silversmith Hammer", ConstPrice.Ng12700, EquipmentGrade.Ng,0)},
            {ItemId.DwarvenMace, baseItem => InitItem(new OneHandBlunt(17, 12), baseItem, "Silversmith Hammer", ConstPrice.Ng27k, EquipmentGrade.Ng,0)},
            {ItemId.BoneClub, baseItem => InitItem(new OneHandBlunt(24, 17), baseItem, "Bone Club", ConstPrice.Ng68k, EquipmentGrade.Ng,0)},
            {ItemId.Buzdygan, baseItem => InitItem(new OneHandBlunt(31, 21), baseItem, "Buzdygan", ConstPrice.Ng122k, EquipmentGrade.Ng,0)},
            {ItemId.IronHammer, baseItem => InitItem(new OneHandBlunt(31, 21), baseItem, "Iron Hammer", ConstPrice.Ng122k, EquipmentGrade.Ng,0)},
            //1h blunt D
            {ItemId.HandAxe, baseItem => InitItem(new OneHandBlunt(40, 26), baseItem, "Hand Axe", ConstPrice.D205k, EquipmentGrade.D,ConstCrystal.D743)},
            {ItemId.HeavyMace, baseItem => InitItem(new OneHandBlunt(40, 26), baseItem, "Heavy Mace", ConstPrice.D205k, EquipmentGrade.D,ConstCrystal.D743)},

            //------------------
            //Dagger NG
            {ItemId.BoneDagger, baseItem => InitItem(new Dagger(7, 6), baseItem, "Bone Dagger", ConstPrice.Ng384, EquipmentGrade.Ng,0)},
            //Dagger D
            {ItemId.AssassinKnife, baseItem => InitItem(new Dagger(35, 26), baseItem, "Assassin Knife", ConstPrice.D205k, EquipmentGrade.D,ConstCrystal.D743)},

            //------------------
            //Bow NG
            {ItemId.ShortBow, baseItem => InitItem(new Bow(16, 6), baseItem, "Short Bow", ConstPrice.Ng384, EquipmentGrade.Ng,0)},
            //Bow D
            {ItemId.StrengthenedBow, baseItem => InitItem(new Bow(82, 26), baseItem, "Strengthened Bow", ConstPrice.D205k, EquipmentGrade.D,ConstCrystal.D743)},

            //------------------
            //Dual swords D
            {ItemId.SaberXSaber, baseItem => InitItem(new DualSword(73, 37), baseItem, "Saber * Saber", ConstPrice.D439k, EquipmentGrade.D,ConstCrystal.D1594)},

            //------------------
            //Dual fists NG
            {ItemId.SpikedGloves, baseItem => InitItem(new DualFist(10, 6), baseItem, "Spiked Gloves", ConstPrice.Ng384, EquipmentGrade.Ng,0)},
            //Dual fists D
            {ItemId.BaghNakh, baseItem => InitItem(new DualFist(49, 26), baseItem, "Bagh-Nakh", ConstPrice.D205k, EquipmentGrade.D,ConstCrystal.D743)},

            //------------------
            //Polearm NG
            {ItemId.ShortSpear, baseItem => InitItem(new Polearm(24, 17), baseItem, "Short Spear", ConstPrice.Ng68k, EquipmentGrade.Ng,0)},
            //Polearm D
            {ItemId.Trident, baseItem => InitItem(new Polearm(40, 26), baseItem, "Trident", ConstPrice.D205k, EquipmentGrade.D,ConstCrystal.D743)},

            //------------------
            //1h sword NG
            {ItemId.SquiresSword, baseItem => InitItem(new OneHandSword(6, 5), baseItem, "Squire's Sword", 0, EquipmentGrade.Ng,0)},
            {ItemId.ShortSword, baseItem => InitItem(new OneHandSword(8, 6), baseItem, "Short Sword", ConstPrice.Ng69, EquipmentGrade.Ng,0)},
            //1h sword D
            {ItemId.Saber, baseItem => InitItem(new OneHandSword(40, 26), baseItem, "Saber", ConstPrice.D205k, EquipmentGrade.D,ConstCrystal.D743)},

            //------------------
            //2h sword NG
            {ItemId.RedSunsetSword, baseItem => InitItem(new TwoHandSword(16, 10), baseItem, "Red Sunset Sword", ConstPrice.Ng12700, EquipmentGrade.Ng,0)},
            //2h sword D
            {ItemId.TwoHandedSword, baseItem => InitItem(new TwoHandSword(78, 39), baseItem, "Two-Handed Sword", ConstPrice.D484k, EquipmentGrade.D,ConstCrystal.D1758)},

            
            //------------------
            //2h blunt NG
            {ItemId.WillowStaff, baseItem => InitItem(new TwoHandBlunt(11, 12), baseItem, "Willow Staff", ConstPrice.Ng6250, EquipmentGrade.Ng,0)},
            //2h blunt D
            {ItemId.BoneStaff, baseItem => InitItem(new TwoHandBlunt(39, 35), baseItem, "Bone Staff", ConstPrice.D205k, EquipmentGrade.D,ConstCrystal.D743)},

            //------------------
            //Ring NG
            {ItemId.MagicRing, baseItem => InitItem(new Ring(false, 7), baseItem, "Magic Ring", ConstPrice.Ng17, EquipmentGrade.Ng,0)},
            //Ring D
            {ItemId.RingOfDevotion, baseItem => InitItem(new Ring(false, 16), baseItem, "Ring of Devotion", ConstPrice.D9k, EquipmentGrade.D,ConstCrystal.D32)},

            //------------------
            //Earring NG
            {ItemId.ApprenticeEarring, baseItem => InitItem(new Earring(false, 11), baseItem, "Apprentice's Earring", ConstPrice.Ng17, EquipmentGrade.Ng,0)},
            //Earring D
            {ItemId.RedCrescentEarring, baseItem => InitItem(new Earring(false, 24), baseItem, "Red Crescent Earring", ConstPrice.D9k, EquipmentGrade.D,ConstCrystal.D32)},

            //------------------
            //Neck NG
            {ItemId.NecklaceOfMagic, baseItem => InitItem(new Necklace(false, 15), baseItem, "Necklace of Magic", ConstPrice.Ng17, EquipmentGrade.Ng,0)},
            //Neck D
            {ItemId.NecklaceOfDevotion, baseItem => InitItem(new Necklace(false, 32), baseItem, "Necklace of Devotion", ConstPrice.D9k, EquipmentGrade.D,ConstCrystal.D32)},

            //------------------
            //Heavy body NG
            {ItemId.PieceBoneBreastplate, baseItem => InitItem(new BodyArmor(ArmorKind.Heavy, 101), baseItem, "Piece Bone Breastplate", ConstPrice.Ng27k, EquipmentGrade.Ng,0)},
            //Heavy body D
            {ItemId.RingMailBreastplate, baseItem => InitItem(new BodyArmor(ArmorKind.Heavy, 125), baseItem, "Ring Mail Breastplate", 67000, EquipmentGrade.D,245)},

            //------------------
            //Light body NG
            {ItemId.SquireShirt, baseItem => InitItem(new BodyArmor(ArmorKind.Light, 53), baseItem, "Squire's Shirt", ConstPrice.Ng17, EquipmentGrade.Ng,0)},
            {ItemId.LeatherShirt, baseItem => InitItem(new BodyArmor(ArmorKind.Light, 70), baseItem, "Leather Shirt", 2000, EquipmentGrade.Ng,0)},
            //Light body D
            {ItemId.PumaSkinShirt, baseItem => InitItem(new BodyArmor(ArmorKind.Light, 94), baseItem, "Puma Skin Shirt", 52000, EquipmentGrade.D,184)},

            //------------------
            //Magic body NG
            {ItemId.ApprenticeTunic, baseItem => InitItem(new BodyArmor(ArmorKind.Magic, 27), baseItem, "Apprentice's Tunic", ConstPrice.Ng17, EquipmentGrade.Ng,0)},
            {ItemId.Tunic, baseItem => InitItem(new BodyArmor(ArmorKind.Magic, 29), baseItem, "Tunic", ConstPrice.Ng69, EquipmentGrade.Ng,0)},
            //Magic body D
            {ItemId.CursedTunic, baseItem => InitItem(new BodyArmor(ArmorKind.Magic, 63), baseItem, "Cursed Tunic", 41000, EquipmentGrade.D,184)},

            //------------------
            //Boots NG
            {ItemId.ClothShoes, baseItem => InitItem(new Boots(9), baseItem, "Cloth Shoes", 9, EquipmentGrade.Ng,0)},
            //Boots D
            {ItemId.LeatherBoots, baseItem => InitItem(new Boots(19), baseItem, "Leather Boots", 11000, EquipmentGrade.D,38)},

            //------------------
            //Gloves NG
            {ItemId.ShortGloves, baseItem => InitItem(new Gloves(9), baseItem, "Short Gloves", 9, EquipmentGrade.Ng,0)},
            //Gloves D
            {ItemId.CraftedLeatherGloves, baseItem => InitItem(new Gloves(19), baseItem, "Crafted Leather Gloves", 11000, EquipmentGrade.D,38)},

            //------------------
            //Helmet NG
            {ItemId.ClothCap, baseItem => InitItem(new Helmet(13), baseItem, "Cloth Cap", 25, EquipmentGrade.Ng,0)},
            //Helmet D
            {ItemId.BoneHelmet, baseItem => InitItem(new Helmet(29), baseItem, "Bone Helmet", 15000, EquipmentGrade.D,56)},
        };

        [NotNull]
        internal static IEquipment Create(ItemId itemId)
        {
            return Create(new BaseItem
            {
                EnchantLevel = 0,
                GuidKey = Guid.NewGuid(),
                Id = itemId
            });
        }

        [NotNull]
        internal static IEquipment Create(ItemId itemId, int enchantLevel)
        {
            return Create(new BaseItem
            {
                EnchantLevel = enchantLevel,
                GuidKey = Guid.NewGuid(),
                Id = itemId
            });
        }

        [NotNull]
        internal static IEquipment Create(BaseItem baseItem)
        {
            if (Mapping.ContainsKey(baseItem.Id))
                return Mapping[baseItem.Id](baseItem);

            throw new InvalidOperationException($"Item is not supported by factory: {baseItem.Id}");
        }

        [NotNull]
        private static IEquipment InitItem([NotNull] IEquipment item, [NotNull] BaseItem baseItem, 
            string name, int basePrice, EquipmentGrade grade, int crystals)
        {
            item.BasePrice = basePrice;
            item.Grade = grade;
            item.GuidKey = baseItem.GuidKey;
            item.Name = name;
            item.Id = baseItem.Id;
            item.EnchantInfo = grade == EquipmentGrade.Ng
                ? EnchantInfo.EnchantNotSupported
                : CreateEnchantInfo(item, crystals, baseItem.EnchantLevel);

            return item;
        }

        private static IEnchantInfo CreateEnchantInfo(IEquipment item, int crystals, int enchantLevel)
        {
            var enchantFunc = item switch
            {
                JewelryAccessorie _ => EnchantFunctions.Jewelry,
                BodyArmor _ => EnchantFunctions.BodyArmor,
                Boots _ => EnchantFunctions.SideArmor,
                Gloves _ => EnchantFunctions.SideArmor,
                Helmet _ => EnchantFunctions.SideArmor,
                _ => null
            };

            if(enchantFunc is null)
                switch (item.Grade)
                {
                    case EquipmentGrade.Ng:
                        return EnchantInfo.EnchantNotSupported;
                    case EquipmentGrade.D:
                        enchantFunc = item switch
                        {
                            Bow _ => EnchantFunctions.BowD,
                            OneHandBlunt _ => EnchantFunctions.Weapon1hD,
                            OneHandSword _ => EnchantFunctions.Weapon1hD,
                            Dagger _ => EnchantFunctions.Weapon1hD,
                            Polearm _ => EnchantFunctions.Weapon1hD,
                            TwoHandBlunt _ => EnchantFunctions.Weapon2hD,
                            TwoHandSword _ => EnchantFunctions.Weapon2hD,
                            DualFist _ => EnchantFunctions.Weapon2hD,
                            DualSword _ => EnchantFunctions.Weapon2hD,
                            _ => throw new ArgumentOutOfRangeException(nameof(item))
                        };
                        break;

                    case EquipmentGrade.C:
                        enchantFunc = item switch
                        {
                            Bow _ => EnchantFunctions.BowC,
                            OneHandBlunt _ => EnchantFunctions.Weapon1hC,
                            OneHandSword _ => EnchantFunctions.Weapon1hC,
                            Dagger _ => EnchantFunctions.Weapon1hC,
                            Polearm _ => EnchantFunctions.Weapon1hC,
                            TwoHandBlunt _ => EnchantFunctions.Weapon2hC,
                            TwoHandSword _ => EnchantFunctions.Weapon2hC,
                            DualFist _ => EnchantFunctions.Weapon2hC,
                            DualSword _ => EnchantFunctions.Weapon2hC,
                            _ => throw new ArgumentOutOfRangeException(nameof(item))
                        };
                        break;

                    case EquipmentGrade.B:
                        enchantFunc = item switch
                        {
                            Bow _ => EnchantFunctions.BowB,
                            OneHandBlunt _ => EnchantFunctions.Weapon1hB,
                            OneHandSword _ => EnchantFunctions.Weapon1hB,
                            Dagger _ => EnchantFunctions.Weapon1hB,
                            Polearm _ => EnchantFunctions.Weapon1hB,
                            TwoHandBlunt _ => EnchantFunctions.Weapon2hB,
                            TwoHandSword _ => EnchantFunctions.Weapon2hB,
                            DualFist _ => EnchantFunctions.Weapon2hB,
                            DualSword _ => EnchantFunctions.Weapon2hB,
                            _ => throw new ArgumentOutOfRangeException(nameof(item))
                        };
                        break;

                    case EquipmentGrade.A:
                        enchantFunc = item switch
                        {
                            Bow _ => EnchantFunctions.BowA,
                            OneHandBlunt _ => EnchantFunctions.Weapon1hA,
                            OneHandSword _ => EnchantFunctions.Weapon1hA,
                            Dagger _ => EnchantFunctions.Weapon1hA,
                            Polearm _ => EnchantFunctions.Weapon1hA,
                            TwoHandBlunt _ => EnchantFunctions.Weapon2hA,
                            TwoHandSword _ => EnchantFunctions.Weapon2hA,
                            DualFist _ => EnchantFunctions.Weapon2hA,
                            DualSword _ => EnchantFunctions.Weapon2hA,
                            _ => throw new ArgumentOutOfRangeException(nameof(item))
                        };
                        break;

                    case EquipmentGrade.S:
                        enchantFunc = item switch
                        {
                            Bow _ => EnchantFunctions.BowS,
                            OneHandBlunt _ => EnchantFunctions.Weapon1hS,
                            OneHandSword _ => EnchantFunctions.Weapon1hS,
                            Dagger _ => EnchantFunctions.Weapon1hS,
                            Polearm _ => EnchantFunctions.Weapon1hS,
                            TwoHandBlunt _ => EnchantFunctions.Weapon2hS,
                            TwoHandSword _ => EnchantFunctions.Weapon2hS,
                            DualFist _ => EnchantFunctions.Weapon2hS,
                            DualSword _ => EnchantFunctions.Weapon2hS,
                            _ => throw new ArgumentOutOfRangeException(nameof(item))
                        };
                        break;

                    default:
                        throw new ArgumentOutOfRangeException(nameof(item));
                }

            return new EnchantInfo(enchantFunc, crystals)
            {
                EnchantLevel = enchantLevel
            };
        }
    }
}
