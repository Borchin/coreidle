﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using CoreIdle.Character;

// ReSharper disable UnusedMember.Global

namespace CoreIdle.Inventory
{
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    internal static class EnchantFunctions
    {
        [JetBrains.Annotations.NotNull]
        internal static Func<int, List<StatChange>> Weapon1hD = i => new List<StatChange>
        {
            new StatChange(Stat.Patk, StatCalcAction.AddToBase, i < 4 ? i * 2 : i * 4),
            new StatChange(Stat.Matk, StatCalcAction.AddToBase, i < 4 ? i * 2 : i * 4),
        };

        [JetBrains.Annotations.NotNull]
        internal static Func<int, List<StatChange>> Weapon2hD = i => new List<StatChange>
        {
            new StatChange(Stat.Patk, StatCalcAction.AddToBase, i < 4 ? i * 3 : i * 6),
            new StatChange(Stat.Matk, StatCalcAction.AddToBase, i < 4 ? i * 2 : i * 4),
        };

        [JetBrains.Annotations.NotNull]
        internal static Func<int, List<StatChange>> BowD = i => new List<StatChange>
        {
            new StatChange(Stat.Patk, StatCalcAction.AddToBase, i < 4 ? i * 4 : i * 8),
            new StatChange(Stat.Matk, StatCalcAction.AddToBase, i < 4 ? i * 2 : i * 4),
        };

        [JetBrains.Annotations.NotNull]
        internal static Func<int, List<StatChange>> Weapon1hC = i => new List<StatChange>
        {
            new StatChange(Stat.Patk, StatCalcAction.AddToBase, i < 4 ? i * 3 : i * 6),
            new StatChange(Stat.Matk, StatCalcAction.AddToBase, i < 4 ? i * 3 : i * 6),
        };

        [JetBrains.Annotations.NotNull]
        internal static Func<int, List<StatChange>> Weapon2hC = i => new List<StatChange>
        {
            new StatChange(Stat.Patk, StatCalcAction.AddToBase, i < 4 ? i * 4 : i * 8),
            new StatChange(Stat.Matk, StatCalcAction.AddToBase, i < 4 ? i * 3 : i * 6),
        };

        [JetBrains.Annotations.NotNull]
        internal static Func<int, List<StatChange>> BowC = i => new List<StatChange>
        {
            new StatChange(Stat.Patk, StatCalcAction.AddToBase, i < 4 ? i * 6 : i * 12),
            new StatChange(Stat.Matk, StatCalcAction.AddToBase, i < 4 ? i * 3 : i * 6),
        };

        [JetBrains.Annotations.NotNull]
        internal static Func<int, List<StatChange>> Weapon1hB = i => new List<StatChange>
        {
            new StatChange(Stat.Patk, StatCalcAction.AddToBase, i < 4 ? i * 4 : i * 8),
            new StatChange(Stat.Matk, StatCalcAction.AddToBase, i < 4 ? i * 4 : i * 8),
        };

        [JetBrains.Annotations.NotNull]
        internal static Func<int, List<StatChange>> Weapon2hB = i => new List<StatChange>
        {
            new StatChange(Stat.Patk, StatCalcAction.AddToBase, i < 4 ? i * 6 : i * 12),
            new StatChange(Stat.Matk, StatCalcAction.AddToBase, i < 4 ? i * 4 : i * 8),
        };

        [JetBrains.Annotations.NotNull]
        internal static Func<int, List<StatChange>> BowB = i => new List<StatChange>
        {
            new StatChange(Stat.Patk, StatCalcAction.AddToBase, i < 4 ? i * 8 : i * 16),
            new StatChange(Stat.Matk, StatCalcAction.AddToBase, i < 4 ? i * 4 : i * 8),
        };

        [JetBrains.Annotations.NotNull]
        internal static Func<int, List<StatChange>> Weapon1hA = i => new List<StatChange>
        {
            new StatChange(Stat.Patk, StatCalcAction.AddToBase, i < 4 ? i * 5 : i * 10),
            new StatChange(Stat.Matk, StatCalcAction.AddToBase, i < 4 ? i * 5 : i * 10),
        };

        [JetBrains.Annotations.NotNull]
        internal static Func<int, List<StatChange>> Weapon2hA = i => new List<StatChange>
        {
            new StatChange(Stat.Patk, StatCalcAction.AddToBase, i < 4 ? i * 7 : i * 14),
            new StatChange(Stat.Matk, StatCalcAction.AddToBase, i < 4 ? i * 5 : i * 10),
        };

        [JetBrains.Annotations.NotNull]
        internal static Func<int, List<StatChange>> BowA = i => new List<StatChange>
        {
            new StatChange(Stat.Patk, StatCalcAction.AddToBase, i < 4 ? i * 10 : i * 20),
            new StatChange(Stat.Matk, StatCalcAction.AddToBase, i < 4 ? i * 5 : i * 10),
        };

        [JetBrains.Annotations.NotNull]
        internal static Func<int, List<StatChange>> Weapon1hS = i => new List<StatChange>
        {
            new StatChange(Stat.Patk, StatCalcAction.AddToBase, i < 4 ? i * 8 : i * 16),
            new StatChange(Stat.Matk, StatCalcAction.AddToBase, i < 4 ? i * 6 : i * 12),
        };

        [JetBrains.Annotations.NotNull]
        internal static Func<int, List<StatChange>> Weapon2hS = i => new List<StatChange>
        {
            new StatChange(Stat.Patk, StatCalcAction.AddToBase, i < 4 ? i * 12 : i * 24),
            new StatChange(Stat.Matk, StatCalcAction.AddToBase, i < 4 ? i * 6 : i * 12),
        };

        [JetBrains.Annotations.NotNull]
        internal static Func<int, List<StatChange>> BowS = i => new List<StatChange>
        {
            new StatChange(Stat.Patk, StatCalcAction.AddToBase, i < 4 ? i * 16 : i * 32),
            new StatChange(Stat.Matk, StatCalcAction.AddToBase, i < 4 ? i * 6 : i * 12),
        };

        [JetBrains.Annotations.NotNull]
        internal static Func<int, List<StatChange>> SideArmor = i => new List<StatChange>
        {
            new StatChange(Stat.Pdef, StatCalcAction.AddToBase, i < 4 ? i * 1 : i * 3),
        };

        [JetBrains.Annotations.NotNull]
        internal static Func<int, List<StatChange>> BodyArmor = i => new List<StatChange>
        {
            new StatChange(Stat.Pdef, StatCalcAction.AddToBase, i < 4 ? i * 1 : i * 3),
        };

        [JetBrains.Annotations.NotNull]
        internal static Func<int, List<StatChange>> Jewelry = i => new List<StatChange>
        {
            new StatChange(Stat.Mdef, StatCalcAction.AddToBase, i < 4 ? i * 1 : i * 3),
        };
    }
}
