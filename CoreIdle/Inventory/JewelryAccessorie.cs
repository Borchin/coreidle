﻿using System;
using System.Linq;
using CoreIdle.Character;

namespace CoreIdle.Inventory
{
    internal abstract class JewelryAccessorie : Equipement
    {
        protected JewelryAccessorie(string itemClassName, bool unique, int mDef) : base(itemClassName)
        {
            Unique = unique;
            StatChanges.Add(new StatChange(Stat.Mdef, StatCalcAction.AddToBase, mDef));
        }

        internal bool Unique { get; }
        internal int Mdef => Convert.ToInt32(SummaryStats.Where(s => s.Stat == Stat.Mdef).Sum(s => s.Value));

        public override string ToString()
        {
            return $"{base.ToString()}\n Unique:{Unique} Mdef:{Mdef}";
        }
    }

    internal sealed class Ring : JewelryAccessorie
    {
        public Ring(bool unique, int mDef) : base("Ring", unique, mDef) { }
    }

    internal sealed class Earring : JewelryAccessorie
    {
        public Earring(bool unique, int mDef) : base("Earring", unique, mDef) { }
    }

    internal sealed class Necklace : JewelryAccessorie
    {
        public Necklace(bool unique, int mDef) : base("Necklace", unique, mDef) { }
    }
}
