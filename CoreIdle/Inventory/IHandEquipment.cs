﻿namespace CoreIdle.Inventory
{
    internal interface IHandEquipment : IEquipment
    {
        HandUsage Hand { get; }
    }
}
