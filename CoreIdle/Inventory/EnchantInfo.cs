﻿using System;
using System.Collections.Generic;
using CoreIdle.Character;
using JetBrains.Annotations;

namespace CoreIdle.Inventory
{
    internal interface IEnchantInfo
    {
        int EnchantLevel { get; set; }
        List<StatChange> EnchantBonuses { get; }
        int CrystalAmount { get; }
    }

    internal sealed class EnchantInfo : IEnchantInfo
    {
        [NotNull] private readonly Func<int, List<StatChange>> _enchantFunction;
        private readonly int _baseCrystalAmount;

        public int EnchantLevel { get; set; }
        public int CrystalAmount => Convert.ToInt32(_baseCrystalAmount * Math.Pow(1.1, EnchantLevel));

        [NotNull] public List<StatChange> EnchantBonuses => _enchantFunction(EnchantLevel);

        internal EnchantInfo([NotNull] Func<int, List<StatChange>> enchantFunction, int baseCrystalAmount)
        {
            _enchantFunction = enchantFunction ?? throw new ArgumentNullException(nameof(enchantFunction));
            _baseCrystalAmount = baseCrystalAmount;
            EnchantLevel = 0;
        }

        public override string ToString()
        {
            return $"EnchantLevel:{EnchantLevel} CrystalAmount:{CrystalAmount}";
        }

        internal static IEnchantInfo EnchantNotSupported { get; } = new Unenchantable();

        private sealed class Unenchantable : IEnchantInfo
        {
            public int EnchantLevel
            {
                get => throw new NotImplementedException("EnchantNotSupported");
                set => throw new NotImplementedException("EnchantNotSupported");
            }
            public List<StatChange> EnchantBonuses => throw new NotImplementedException("EnchantNotSupported");
            public int CrystalAmount => throw new NotImplementedException("EnchantNotSupported");

            public override string ToString()
            {
                return "EnchantNotSupported";
            }
        }
    }
}
