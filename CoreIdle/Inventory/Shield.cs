﻿using CoreIdle.Character;

namespace CoreIdle.Inventory
{
    internal class Shield : Equipement, IHandEquipment
    {
        public Shield(int blockValue, int blockChance) : base("Shield")
        {
            Hand = HandUsage.Left;
            StatChanges.Add(new StatChange(Stat.ShieldBlockChance, StatCalcAction.AddToBase, blockChance));
            StatChanges.Add(new StatChange(Stat.ShieldBlockValue, StatCalcAction.AddToBase, blockValue));
            StatChanges.Add(new StatChange(Stat.Evasion, StatCalcAction.AddToBase, -8));
        }

        public HandUsage Hand { get; }
    }
}
