﻿using System;
using System.Collections.Generic;
using CoreIdle.Character;

namespace CoreIdle
{
    [Serializable]
    public sealed class GameServerState
    {
        public string Name { get; set; }
        public double ItemDropRate { get; set; }
        public double GoldDropRate { get; set; }
        public double XpRate { get; set; }
        public Squad PlayerSquad { get; set; }
        public List<BaseUnit> Players { get; set; } = new List<BaseUnit>();
    }
}
