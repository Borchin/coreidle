﻿using System;

namespace CoreIdle
{
    internal static class GameStateFactory
    {
        internal static GameState Crate()
        {
            var result = new GameState();
            result.Currency.Add(CurrencyType.RealMoney, 10);
            result.Currency.Add(CurrencyType.Gold, 0);

            result.GameServer = null;
            result.Features = new FeaturesState
            {
                GlobalUpgrades = true,
                ManageAdventures = false,
                ManageParty = false,
                SelectNewServer = true
            };
            result.GameTime = new DateTime(1, 1, 1);

            return result;
        }
    }
}
