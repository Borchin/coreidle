﻿using System;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace CoreIdle
{
    internal sealed class MainManager
    {
        internal event EventHandler FeatureChanged;
        private void OnFeatureChanged()
        {
            FeatureChanged?.Invoke(this, EventArgs.Empty);
        }

        [NotNull] private readonly GameServerManager _gameServerManager;

        [NotNull] internal GameState CurrentGameState { get; }
        [NotNull] internal GameLoopManager GameLoopManager { get; } = new GameLoopManager();

        public MainManager(string saveName, [NotNull] GameState currentGameState)
        {
            CurrentGameState = currentGameState ?? throw new ArgumentNullException(nameof(currentGameState));
            if (string.IsNullOrWhiteSpace(saveName)) throw new ArgumentNullException(nameof(saveName));

            _gameServerManager = new GameServerManager();
            
            var saver = new Saver(saveName, CurrentGameState);
            GameLoopManager.Update += (sender, delta) =>
            {
                _gameServerManager.OnUpdate(delta);
                saver.OnUpdate(delta);
            };

            if (currentGameState.GameServer != null)
                _gameServerManager.Start();

            if (currentGameState.GameServer != null && !currentGameState.GameServer.PlayerSquad.Members.TrueForAll(a => currentGameState.GameServer.Players.Contains(a)))
                throw new InvalidOperationException("AAAAAAAAAAAAA");

            if (currentGameState.GameServer != null)
            {
                var a = currentGameState.GameServer.PlayerSquad.Members
                    .Where(unit => currentGameState.GameServer.Players.Contains(unit)).ToList();
                foreach (var baseUnit in a)
                {
                    Debug.WriteLine(baseUnit.Name);
                }
            }
        }

        internal void PickServer([NotNull] GameServerState gameServerState)
        {
            if(gameServerState == null) throw new ArgumentNullException(nameof(gameServerState));

            CurrentGameState.GameServer = gameServerState;
            CurrentGameState.Features.ManageAdventures = true;
            CurrentGameState.Features.ManageParty = true;
            _gameServerManager.Start();
            OnFeatureChanged();
        }
    }
}
