﻿using System;
using CoreIdle.Character;

namespace CoreIdle
{
    internal static class TextHelper
    {
        internal static string GetStatName(Stat stat)
        {
            return stat switch
            {
                Stat.MaxHp => "HP",
                Stat.HpPerLevel => "HP per level",
                Stat.CurrentHp => "Current HP",
                Stat.Patk => "Physical attack",
                Stat.Matk => "Magical attack",
                Stat.Pdef => "Physical defence",
                Stat.Mdef => "Magical defence",
                Stat.Accuracy => "Accuracy",
                Stat.Evasion => "Evasion",
                Stat.PCritChance => "Physical crit. chance",
                Stat.PCritPower => "Physical crit. power",
                Stat.MCritChance => "Magical crit. chance",
                Stat.MCritPower => "Magical crit. power",
                Stat.AtkSpeed => "Attack speed",
                Stat.CastSpeed => "Casting speed",
                Stat.InterruptResist => "Chance to resist interrupt casting",
                Stat.StunResist => "Chance to resist stun",
                Stat.DebuffResist => "Chance to resist negative effect",
                Stat.FireResist => "Fire resist",
                Stat.WaterResist => "Water resist",
                Stat.WindResist => "Wind resist",
                Stat.DarkResist => "Dark resist",
                Stat.HolyResist => "Holy resist",
                Stat.PoisonResist => "Poison resist",
                Stat.BleedResist => "Bleed resist",
                Stat.HealingBonus => "Healing magic power",
                Stat.HealReceivedBonus => "Healing received",
                Stat.HpRecovery => "HP per 5 seconds",
                Stat.ShieldBlockValue => "Bonus defence on block",
                Stat.ShieldBlockChance => "Chance to block damage with shield",
                Stat.CooldownRecoveryMultiply => "Skill recovery speed rating",
                _ => throw new ArgumentOutOfRangeException(nameof(stat), stat, null)
            };
        }

        internal static string GetRaceName(UnitRace race)
        {
            return race switch
            {
                UnitRace.Human => "Human",
                UnitRace.Dwarf => "Dwarf",
                UnitRace.Elf => "Elf",
                UnitRace.DarkElf => "Dark Elf",
                UnitRace.Orc => "Orc",
                UnitRace.Undead => "Undead",
                UnitRace.Construct => "Construct",
                UnitRace.Beast => "Beast",
                UnitRace.Animal => "Animal",
                UnitRace.Plant => "Plant",
                UnitRace.Humanoid => "Humanoid",
                UnitRace.Spirit => "Spirit",
                UnitRace.Divine => "Divine",
                UnitRace.Demonic => "Demonic",
                UnitRace.Dragon => "Dragon",
                UnitRace.Giant => "Giant",
                UnitRace.Bug => "Bug",
                UnitRace.Fairy => "Fairy",
                _ => throw new ArgumentOutOfRangeException(nameof(race), race, null)
            };
        }

        public static string GetClassName(UnitClass baseUnitUnitClass)
        {
            return baseUnitUnitClass switch
            {
                UnitClass.HumanMystic => "Mystic",
                UnitClass.ElvenMystic => "Mystic",
                UnitClass.DarkElvenMystic => "Mystic",
                UnitClass.OrcMystic => "Mystic",
                UnitClass.HumanFighter => "Fighter",
                UnitClass.ElvenFighter => "Fighter",
                UnitClass.DarkElvenFighter => "Fighter",
                UnitClass.OrcFighter => "Fighter",
                UnitClass.DwarfFighter => "Fighter",
                _ => throw new ArgumentOutOfRangeException(nameof(baseUnitUnitClass), baseUnitUnitClass, null)
            };
        }
    }
}
