﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Windows.Threading;

namespace CoreIdle
{
    internal sealed class GameLoopManager
    {
        internal event EventHandler<TimeSpan> Update;
        private void OnUpdate(TimeSpan e)
        {
            Update?.Invoke(this, e);
        }

        private readonly Stopwatch _stopWatch = new Stopwatch();
        private readonly DispatcherTimer _timer = new DispatcherTimer(DispatcherPriority.Send);

        internal bool GameIsPaused { get; set; }

        internal GameLoopManager()
        {
            _timer.Interval = TimeSpan.FromSeconds(0.1);
            _timer.Tick += TimerOnTick;
            _stopWatch.Start();
            _timer.Start();
        }

        private void TimerOnTick(object sender, EventArgs e)
        {
            var delta = _stopWatch.Elapsed;
            _stopWatch.Restart();

            OnUpdate(GameIsPaused ? TimeSpan.Zero : delta);
        }
    }
}
