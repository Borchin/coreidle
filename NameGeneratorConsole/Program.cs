﻿using CoreIdle.NameGenerator;

namespace NameGeneratorConsole
{
    internal static class Program
    {
        private static void Main()
        {
            var list = string.Empty;
            for (var i = 0; i < 100; i++)
            {
                list += "\n" + NameGenerator.Generate();
            }
            NotePadHelper.ShowMessage(list);
        }
    }
}
